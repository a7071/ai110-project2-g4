﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class ProductLineBusiness
    {
        public void CreateProductLine(ProductLine productLine)
        {
            ProductLineDAO dao = new ProductLineDAO();
            dao.Create(productLine);
        }
        public ProductLine GetProductLine(int id)
        {
            ProductLineDAO dao = new ProductLineDAO();
            return dao.GetProductLine(id);
        }

        public ProductLineDetails GetProductLineDetails(int id)
        {
            ProductLineDAO dao = new ProductLineDAO();
            return dao.GetProductLineDetails(id);
        }

        public void Delete(int id)
        {
            ProductLineDAO dao = new ProductLineDAO();
            dao.DeleteProductLine(id);
        }
    }
}
