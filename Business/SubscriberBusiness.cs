﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class SubscriberBusiness
    {
        public Subscriber GetSubscriberById(int id)
        {
            SubscriberDAO subscriberDAO = new SubscriberDAO();
            return subscriberDAO.GetById(id);
        }

        public List<Subscriber> GetSubscribers()
        {
            SubscriberDAO subscriberDAO = new SubscriberDAO();
            return subscriberDAO.GetAll();
        }


    }
}
