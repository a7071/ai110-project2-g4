﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class ScheduleDAO : DAO
    {
        public void InsertPPSchedule(Schedule schedule)
        {
            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO horairesd_ouverture
                                (idJour, idPointDepot, heureD_ouverture, heureFermeture)
                                VALUES
                                (@IdDay, @IdPickupPoint, @OpeningHours, @ClosingHours);";

            //cmd.Parameters.Add(new MySqlParameter("ID", schedule.Id));
            cmd.Parameters.Add(new MySqlParameter("IdDay", schedule.IdDay));
            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", schedule.IdPickupPoint));
            cmd.Parameters.Add(new MySqlParameter("OpeningHours", schedule.OpeningHours));
            cmd.Parameters.Add(new MySqlParameter("ClosingHours", schedule.ClosingHours));
            
            try
            {
                cnx.Open();

                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion dans la base d'un horaire." + exc.Message);
            }
            finally
            {
                cnx.Close();

            }
        }

        public void Update(Schedule schedule)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE horairesd_ouverture
                                SET idJour = @title,
                                    idPointDepot = @description
                                    heureD_ouverture
                                    heureFermeture
                                WHERE idHoraire = @idSchedule";
            cmd.Parameters.Add(new MySqlParameter("IdDay", schedule.IdDay));
            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", schedule.IdPickupPoint));
            cmd.Parameters.Add(new MySqlParameter("OpeningHours", schedule.OpeningHours));
            cmd.Parameters.Add(new MySqlParameter("ClosingHours", schedule.ClosingHours));
            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur de la modification d'un horaire : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

        }

        private Schedule DataReaderToEntity(DbDataReader dr)
        {
            Schedule pickupSchedule = new Schedule();

            pickupSchedule.Id = dr.GetInt32(dr.GetOrdinal("idHoraires"));
            pickupSchedule.IdDay = dr.GetInt32(dr.GetOrdinal("idJour"));
            pickupSchedule.IdPickupPoint = dr.GetInt32(dr.GetOrdinal("idPointDepot"));
            if (!dr.IsDBNull(dr.GetOrdinal("heureD_ouverture")))
            {
                pickupSchedule.OpeningHours = dr.GetString(dr.GetOrdinal("heureD_ouverture"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("heureFermeture")))
            {
                pickupSchedule.ClosingHours = dr.GetString(dr.GetOrdinal("heureFermeture"));
            }

            return pickupSchedule;
        }


        public List<ScheduleDetails> GetByIdPickup(int id)
        {
            List<ScheduleDetails> result = new List<ScheduleDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM horairesd_ouverture WHERE idPointDepot = @idPickupPoint";

            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Schedule pickupSchedule = DataReaderToEntity(dr);
                    ScheduleDetails scheduleDetails = new ScheduleDetails(pickupSchedule);
                    result.Add(scheduleDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des horaires d'ouverture : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }



        public List<Schedule> GetAll()
        {
            List<Schedule> result = new List<Schedule>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM horairesd_ouverture";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Schedule pickupSchedule = DataReaderToEntity(dr);
                    result.Add(pickupSchedule);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des horaires d'ouverture : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


    }
}
