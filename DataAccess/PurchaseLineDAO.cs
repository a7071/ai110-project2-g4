﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class PurchaseLineDAO : DAO
    {
        public void Create(PurchaseLine line)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO ligneachat
                                (idAchatCredit,idForfait,quantite)
                                VALUES
                                (@idPurchaseToken,@idTokenBundle,@quantity)";

            cmd.Parameters.Add(new MySqlParameter("idPurchaseToken", line.IdTokenPurchase));
            cmd.Parameters.Add(new MySqlParameter("idTokenBundle", line.IdTokenBundle));
            cmd.Parameters.Add(new MySqlParameter("quantity", line.Quantity));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion d'une ligne d'achat." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public PurchaseLine GetPurchaseLineByPurchaseToken(int idPurchaseToken)
        {
            PurchaseLine result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM ligneachat
                                         WHERE (idAchatCredit = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", idPurchaseToken));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération d'une ligne d'achat" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public PurchaseLine DataReaderToEntity(DbDataReader dr)
        {
            PurchaseLine result = null;
            result.IdTokenPurchase = dr.GetInt32(dr.GetOrdinal("idAchatCredit"));
            result.IdTokenBundle = dr.GetInt32(dr.GetOrdinal("idForfait"));
            result.Quantity = dr.GetInt32(dr.GetOrdinal("quantite"));
            return result;
        }
    }
}
