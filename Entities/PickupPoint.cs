﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class PickupPoint
    {
        public int Id { get; set; }
        public int IdPartner { get; set; }
        public int IdCity { get; set; }
        public string PickupPointName { get; set; }
        public string ?StreetName { get; set; }
        public string StreetNumber { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DeletionDate { get; set; }

        public PickupPoint()
        {
        }

        public PickupPoint(int id, int idPartner, int idCity, 
            string pickupPointName, string streetName, 
            string streetNumber, DateTime creationDate, DateTime deletionDate)
        {
            Id = id;
            IdPartner = idPartner;
            IdCity = idCity;
            PickupPointName = pickupPointName;
            StreetName = streetName;
            StreetNumber = streetNumber;
            CreationDate = creationDate;
            DeletionDate = deletionDate;
        }
    }
}
