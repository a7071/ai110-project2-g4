﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class PostStorageDetails:Post
    {
        public string CityName { get; set; }
        public string ZipCode { get; set; }
        public string StorageFormat { get; set; }
        public string StorageType { get; set; }
        public int IdPickupPoint { get; set; }
        public string PickupPointName { get; set; }
        public string? StreetName { get; set; }
        public string StreetNumber { get; set; }

        public PostStorageDetails(Post post)
        {
            Id = post.Id;
            IdDonatingSubscriber = post.IdDonatingSubscriber;
            IdReceivingSubscriber = post.IdReceivingSubscriber;
            IdStorage = post.IdStorage;
            PostDate = DateTime.Now;
            Title = post.Title;
            Description = post.Description;
            Picture = post.Picture;
            ReservationDate = post.ReservationDate;
            ValidationDate = post.ValidationDate;
            CancellationDate = post.CancellationDate;
            DeletionDate = post.DeletionDate;
            DepositDate = post.DepositDate;
            CollectionDate = post.CollectionDate;
            PinCode = 1234;
            IdCityOfDeposit = post.IdCityOfDeposit;
        }
    }
}
