﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class TokenBundleBusiness
    {
        public List<TokenBundle> GetTokenBundles()
        {
            TokenBundleDAO dao = new TokenBundleDAO();
            return dao.GetTokenBundles();
        }

        public TokenBundle GetTokenBundle(int id)
        {
            TokenBundleDAO dao = new TokenBundleDAO();
            return dao.GetTokenBundle(id);
        }
    }
}
