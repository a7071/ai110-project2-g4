﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class StorageController : Controller
    {
        [HttpGet]
        public IActionResult Index(int idpickuppoint)
        {
                StorageModel model = new StorageModel();
                StorageBusiness storageBusiness = new StorageBusiness();
                StorageTypeBusiness storageTypeBusiness = new StorageTypeBusiness();
                PickupPointBusiness ppbusiness = new PickupPointBusiness();

                model.Types = storageTypeBusiness.GetStorageTypes();
                model.Storages = storageBusiness.ShowStorageDetails(idpickuppoint);
                model.PickupPoint = ppbusiness.GetPickupPoint(idpickuppoint);
                model.PickupDetails = ppbusiness.GetPickupDetailsById(idpickuppoint);

                return View(model);
         
        }

        [HttpGet]
        public IActionResult EditStorage(int idpickuppoint, int idstoragetype)
        {
            StorageModel model = new StorageModel();
            
            VolumeBusiness volbusiness = new VolumeBusiness();
            model.Volumes = volbusiness.GetVolumes();
            
            return View("EditStorage", model);
        }

        [HttpPost]
        public IActionResult EditStorage(StorageModel model, int idpickuppoint, int idstoragetype, int idVolume, int inputNumber)
        {
            StorageBusiness storageBusiness = new StorageBusiness();
            PickupPointBusiness ppbusiness = new PickupPointBusiness();

            model.Storage = new Storage();

            model.PickupPoint = ppbusiness.GetPickupPoint(idpickuppoint);
            model.Storage.IdPickupPoint = idpickuppoint;
            model.Storage.IdStorageType = idstoragetype;
            model.Storage.IdVolume = idVolume;
            model.Storage.CreationDate = storageBusiness.FindCurrentTime();

            for (int i = 0; i < inputNumber; i++)
            {
                storageBusiness.SaveStorage(model.Storage);
                model.Storage.Id = 0;
            }

            return RedirectToAction("Index", "Storage", new {idpickuppoint= idpickuppoint});
        }

        [Route("Storage/Desactivate/{storageId}")]
        public IActionResult Desactivate(int storageId)
        {
            StorageBusiness storageBU = new StorageBusiness();
            storageBU.DesactivateStorage(storageId);
            Storage storage = storageBU.GetStorage(storageId);
            int pickupId = storage.IdPickupPoint;
            return RedirectToAction("Index", "Storage", new { idpickuppoint = pickupId });
        }
    }
}
