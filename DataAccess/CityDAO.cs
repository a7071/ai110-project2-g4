﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class CityDAO : DAO
    {
        private City DataReaderToEntity(DbDataReader dr)
        {
            City city = new City();

            city.Id = dr.GetInt32(dr.GetOrdinal("idVille"));
            city.ZipCode = dr.GetString(dr.GetOrdinal("codePostal"));
            city.CityName = dr.GetString(dr.GetOrdinal("ville"));

            return city;
        }

        public List<City> GetAll()
        {
            List<City> result = new List<City>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM ville";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    City city = DataReaderToEntity(dr);
                    result.Add(city);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des villes : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


        public City GetCity(int id)
        {
            City result = new City();
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = CNX_STR;
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM ville
                                        WHERE (idVille = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de lecture de la base de données." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


    }
}
