﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class SubCategoryBusiness
    {
        public List<SubCategory> GetSubCategoriesByCategory(int idCategory)
        {
            SubCategoryDAO dao = new SubCategoryDAO();
            return dao.GetSubCategoriesByCategories(idCategory);
        }

        public SubCategory GetSubCategory(int id)
        {
            SubCategoryDAO dao = new SubCategoryDAO();
            return dao.GetSubCategory(id);
        }
    }
}
