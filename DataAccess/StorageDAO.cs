using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class StorageDAO:DAO
    {
        public Storage InsertNewStorage(Storage storage)
        {
            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO emplacement
                                (idEmplacement, idVolume, idTypeEmplacement, idPointDepot, dateAjout, dateRetrait, dateAnnulation)
                                VALUES
                                (@ID, @IdVolume, @IdStorageType, @IdPickupPoint, @CreationDate, @DeletionDate, @CancellationDate);
                                SELECT LAST_INSERT_ID();";

            cmd.Parameters.Add(new MySqlParameter("ID", storage.Id));
            cmd.Parameters.Add(new MySqlParameter("IdVolume", storage.IdVolume));
            cmd.Parameters.Add(new MySqlParameter("IdStorageType", storage.IdStorageType));
            cmd.Parameters.Add(new MySqlParameter("IdPickupPoint", storage.IdPickupPoint));
            cmd.Parameters.Add(new MySqlParameter("CreationDate", storage.CreationDate));
            cmd.Parameters.Add(new MySqlParameter("DeletionDate", storage.DeletionDate));
            cmd.Parameters.Add(new MySqlParameter("CancellationDate", storage.CancellationDate));

            try
            {
                cnx.Open();

                storage.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion dans la base d'un emplacement." + exc.Message);
                }
            finally
            {
                cnx.Close();
             }
            return storage;
        }

        public Storage UpdateStorage(Storage storage)
        {
            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE emplacement
                                SET idVolume = @IdVolume, 
                                    idTypeEmplacement = @IdStorageType,
                                    idPointDepot = @IdPickupPoint,
                                    dateAjout = @CreationDate,
                                    dateRetrait = @DeletionDate,
                                    dateAnnulation = @CancellationDate
                                WHERE
                                    idEmplacement = @ID";

            cmd.Parameters.Add(new MySqlParameter("ID", storage.Id));
            cmd.Parameters.Add(new MySqlParameter("IdVolume", storage.IdVolume));
            cmd.Parameters.Add(new MySqlParameter("IdStorageType", storage.IdStorageType));
            cmd.Parameters.Add(new MySqlParameter("IdPickupPoint", storage.IdPickupPoint));
            cmd.Parameters.Add(new MySqlParameter("CreationDate", storage.CreationDate));
            cmd.Parameters.Add(new MySqlParameter("DeletionDate", storage.DeletionDate));
            cmd.Parameters.Add(new MySqlParameter("CancellationDate", storage.CancellationDate));

            try
            {
                cnx.Open();

                storage.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion dans la base d'un emplacement." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return storage;
        }



        public List<int> GetStoragesIdByPickupPoint(int idPickupPoint)
        {
            List<int> result = new List<int>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT e.* FROM emplacement e JOIN pointdedepot p ON e.idPointDepot = p.idPointDepot
                                           WHERE (e.idPointDepot = @idPickuPoint)";
            cmd.Parameters.Add(new MySqlParameter("idPickupPoint",idPickupPoint));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int id = dr.GetInt32(dr.GetOrdinal("idEmplacement"));
                    result.Add(id);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
            }

        public List<StorageDetail> GetStorageVolumeAndTypeByID(int idPickupPt)
        {
            List<StorageDetail> storagedetails = new List<StorageDetail>();

            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT e.*, te.typeEmplacement 'type', v.format
                                FROM emplacement e 
                                LEFT JOIN typed_emplacement te  ON e.idTypeEmplacement = te.idTypeEmplacement
                                LEFT JOIN volume v ON e.idVolume = v.idVolume
                                WHERE idPointDepot = @IDPickupPoint;";

            cmd.Parameters.Add(new MySqlParameter("IDPickupPoint", idPickupPt));

            try
            {
                cnx.Open();

                DbDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    StorageDetail storage = DataReaderToEntity(reader);

                    storage.Type = reader.GetString(reader.GetOrdinal("type"));
                    storage.Format = reader.GetString(reader.GetOrdinal("format"));

                    storagedetails.Add(storage);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de lecture de la table 'typed_emplacement'." + exc.Message);
                }
            finally
            {
                cnx.Close();
            }
            
            return storagedetails;
        }

        public List<Storage> GetStoragesByTypeAndVolume(int idType, int idVolume) 
        {
            List<Storage> result = new List<Storage>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT e.* FROM emplacement e JOIN volume v ON e.idVolume = v.idVolume
                                                              JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement
                                           WHERE (e.idVolume = @idVolume)
                                           AND (e.idTypeEmplacement = @idStorageType)";
            cmd.Parameters.Add(new MySqlParameter("idVolume", idVolume));
            cmd.Parameters.Add(new MySqlParameter("idStorageType", idType));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Storage storage = DataReaderToEntity(dr);
                    result.Add(storage);
           
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
               return result;
        }

        private StorageDetail DataReaderToEntity(DbDataReader reader)
        {
            StorageDetail storage = new StorageDetail();

            storage.Id = reader.GetInt32(reader.GetOrdinal("idEmplacement"));
            storage.IdVolume = reader.GetInt32(reader.GetOrdinal("idVolume"));
            storage.IdPickupPoint = reader.GetInt32(reader.GetOrdinal("idPointDepot"));
            
            if (!reader.IsDBNull(reader.GetOrdinal("idTypeEmplacement")))
            {
                storage.IdStorageType = reader.GetInt32(reader.GetOrdinal("idTypeEmplacement"));
            }

            if (!reader.IsDBNull(reader.GetOrdinal("dateAjout")))
            {
                storage.CreationDate = reader.GetDateTime(reader.GetOrdinal("dateAjout"));
            }
            
            if (!reader.IsDBNull(reader.GetOrdinal("dateRetrait")))
            {
                storage.DeletionDate = reader.GetDateTime(reader.GetOrdinal("dateRetrait"));
            }
            
            if (!reader.IsDBNull(reader.GetOrdinal("dateAnnulation")))
            {
                storage.CancellationDate = reader.GetDateTime(reader.GetOrdinal("dateAnnulation"));
            }

            return storage;
        }

        public Storage GetStorage(int id)
        {
            Storage result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM emplacement
                                        WHERE idEmplacement = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            return result;
        }
        
        public List<Storage> GetStoragesByTypeAndVolumeOfAPickupPoint(int idType, int idVolume, int idPickupPoint)
        {
            List<Storage> result = new List<Storage>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT e.* FROM emplacement e JOIN volume v ON e.idVolume = v.idVolume
                                                              JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement
                                                              JOIN pointdedepot p ON e.idPointDepot = p.idPointDepot
                                           WHERE (e.idVolume = @idVolume)
                                           AND (e.idTypeEmplacement = @idStorageType)
                                           AND (e.idPointDepot = @idPickupPoint)";
            cmd.Parameters.Add(new MySqlParameter("idVolume", idVolume));
            cmd.Parameters.Add(new MySqlParameter("idStorageType", idType));
            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", idPickupPoint));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Storage storage = DataReaderToEntity(dr);
                    result.Add(storage);

                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        
    }
}
