﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PartnerBusiness
    {
        public Partner GetPartnerById(int id)
        {
            PartnerDAO partnerDAO = new PartnerDAO();
            return partnerDAO.GetById(id);
        }


        public List<Partner> GetPartners()
        {
            PartnerDAO partnerDAO = new PartnerDAO();
            return partnerDAO.GetAll();
        }

    }
}
