using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class VolumeBusiness
    {
        public List<Volume> GetVolumes()
        {
            VolumeDAO dao = new VolumeDAO();
            return dao.GetVolumes();
        }

        public Volume GetVolume(int id)
        {
            VolumeDAO dao = new VolumeDAO();
            return dao.GetVolume(id);
        }
    }
}
