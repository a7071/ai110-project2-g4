﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class ProductDAO : DAO
    {
        public Product GetProduct(int id)
        {
            Product result = new Product();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM produit
                                         WHERE idProduit = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Product> GetProducts()
        {
            List<Product> result = new List<Product>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM produit";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Product product = new Product();
                    product = DataReaderToEntity(dr);
                    result.Add(product);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<Product> GetProductByProductType(int idProductType)
        {
            List<Product> result = new List<Product>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT p.* FROM produit p LEFT JOIN typedeproduit tp ON p.idTypeProduit = tp.idTypeProduit
                                            WHERE (p.idTypeProduit = @idProductType OR @idProductType = 0)";
            cmd.Parameters.Add(new MySqlParameter("idProductType", idProductType));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Product product = new Product();
                    product = DataReaderToEntity(dr);
                    result.Add(product);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Product DataReaderToEntity(DbDataReader dr)
        {
            Product product = new Product();

            product.Id = dr.GetInt32(dr.GetOrdinal("idProduit"));
            product.IdProductType = dr.GetInt32(dr.GetOrdinal("idTypeProduit"));
            product.ProductName = dr.GetString(dr.GetOrdinal("nomProduit"));
            return product;
        }
    }
}
