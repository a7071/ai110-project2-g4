﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class SubCategoryDAO : DAO
    {
        public SubCategory GetSubCategory(int id)
        {
            SubCategory result = new SubCategory();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM souscategorie
                                        WHERE idSousCategorie = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            return result;
        }

        public List<SubCategory> getSubCategories()
        {
            List<SubCategory> result = new List<SubCategory>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM souscategorie";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    SubCategory subCategory = new SubCategory();
                    subCategory = DataReaderToEntity(dr);
                    result.Add(subCategory);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<SubCategory> GetSubCategoriesByCategories(int idCategory)
        {
            List<SubCategory> result = new List<SubCategory>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT sc.* FROM souscategorie sc LEFT JOIN categorie c ON sc.idCategorie = c.idCategorie
                                            WHERE (sc.idCategorie = @idCategory OR @idCategory = 0)";
            cmd.Parameters.Add(new MySqlParameter("idCategory", idCategory));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    SubCategory subCategory = new SubCategory();
                    subCategory = DataReaderToEntity(dr);
                    result.Add(subCategory);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public SubCategory DataReaderToEntity(DbDataReader dr)
        {
            SubCategory subCategory = new SubCategory();
            
            subCategory.Id=dr.GetInt32(dr.GetOrdinal("idSousCategorie"));
            subCategory.IdCategory = dr.GetInt32(dr.GetOrdinal("idCategorie"));
            subCategory.SubCategoryName = dr.GetString(dr.GetOrdinal("sousCategorie"));
            return subCategory;
        }
    }
}
