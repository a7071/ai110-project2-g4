using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class StorageTypeDAO:DAO
    {
        public List<int> GetStorageTypeIdsByCategory(int idCategory)
        {
            List<int> result = new List<int>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT q.* FROM categorie c JOIN qualifier q ON c.idCategorie = q.idCategorie 
                                                   WHERE (c.idCategorie = @idCategory)";
            cmd.Parameters.Add(new MySqlParameter("idCategory", idCategory));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int id = dr.GetInt32(dr.GetOrdinal("idTypeEmplacement"));
                    result.Add(id);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
               return result;
        }

        public List<StorageType> GetStorageTypes()
        {
            List<StorageType> result = new List<StorageType>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM typed_emplacement";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    StorageType type = new StorageType();
                    type.Id = dr.GetInt32(dr.GetOrdinal("idTypeEmplacement"));
                    type.Type = dr.GetString(dr.GetOrdinal("typeEmplacement"));
                    result.Add(type);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public StorageType GetStorage(int id)
        {
            StorageType result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM typed_emplacement
                                         WHERE (idTypeEmplacement = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public StorageType DataReaderToEntity(DbDataReader dr)
        {
            StorageType result = new StorageType();
            result.Id = dr.GetInt32(dr.GetOrdinal("idTypeEmplacement"));
            result.Type = dr.GetString(dr.GetOrdinal("typeEmplacement"));
            return result;
        }
    }
}
