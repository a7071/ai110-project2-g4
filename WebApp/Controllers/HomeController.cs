﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        
        
        public IActionResult Dashboard()
        {
            DashboardViewModel model = new DashboardViewModel();
            SubscriberBusiness subBU = new SubscriberBusiness();
            PartnerBusiness partBU = new PartnerBusiness();
            PostBusiness postBU = new PostBusiness();

            model.Subscribers = subBU.GetSubscribers();
            model.Partners = partBU.GetPartners();
            model.AllCompletedExchanges = postBU.GetAllCompleted();
            model.AllOngoingExchanges = postBU.GetAllCurrentExchanges();
            model.AllCancelledPosts = postBU.GetAllCancelled();
            model.AllCompletedExchangesThisMonth = postBU.GetAllCompletedThisMonth();
            model.AllPostsPublishedThisMonth = postBU.GetAllPublishedThisMonth();
            model.AllCancelledThisMonth = postBU.GetAllCancelledThisMonth();


            return View(model);
        }
        
        
        [HttpGet]
        public IActionResult SubscriberDashboard()
        {
            DashboardViewModel model = new DashboardViewModel();
            PostBusiness postBU = new PostBusiness();
            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();

            model.Subscribers = subscriberBU.GetSubscribers();

            model.CurrentPosts = new List<Post>();
            model.UnconfirmedPosts = new List<Post>();
            model.UndepositedPosts = new List<Post>();
            model.CurrentReservations = new List<Post>();
            model.ConfirmedDeposits = new List<Post>();
            model.PastReservations = new List<Post>();

            return View(model);
        }

        [HttpPost]
        public IActionResult SubscriberDashboard(DashboardViewModel model, Subscriber subscriber)
        {
            PostBusiness postBU = new PostBusiness();
            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            model.CurrentPosts = postBU.GetCurrentPosts(subscriber.Id);
            model.NbCurrentPosts = model.CurrentPosts.Count;

            model.UnconfirmedPosts = postBU.GetUnconfirmedPosts(subscriber.Id);
            model.NbUnconfirmedPosts = model.UnconfirmedPosts.Count;

            model.UndepositedPosts = postBU.GetAwaitingDeposit(subscriber.Id);
            model.NbUndepositedPosts = model.UndepositedPosts.Count;

            model.CurrentReservations = postBU.GetCurrentReservations(subscriber.Id);
            model.NbCurrentReservations = model.CurrentReservations.Count;

            model.ConfirmedDeposits = postBU.GetConfirmedDeposits(subscriber.Id);
            model.NbConfirmedDeposits = model.ConfirmedDeposits.Count;

            model.PastReservations = postBU.GetPastReservations(subscriber.Id);
            model.NbPastReservations = model.PastReservations.Count;

            model.PastDonations = postBU.GetPastDonations(subscriber.Id);
            model.NbPastDonations = model.PastDonations.Count;

            return View(model);
        }

        [HttpGet]
        public IActionResult PartnerDashboard()
        {
            DashboardViewModel model = new DashboardViewModel();
            PostBusiness postBU = new PostBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();

            model.Partner = new Partner();
            model.PickupPoints = new List<PickupPointDetails>();
            model.UndepositedPostStorageDetails = new List<PostStorageDetails>();
            model.Partners = partnerBU.GetPartners();

            return View(model);
        }

        [HttpPost]
        public IActionResult PartnerDashboard(DashboardViewModel model, Partner partner)
        {
            PostBusiness postBU = new PostBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();
            PickupPointBusiness ppBU = new PickupPointBusiness();
            StorageBusiness storageBU = new StorageBusiness();

            model.Partners = partnerBU.GetPartners();
            model.PickupPoints = ppBU.GetPickupPointsByIdPartner(partner.Id);
            model.UndepositedPostStorageDetails = postBU.GetPostStorageDetailsAwaitingDepositByPId(partner.Id);
            
            return View(model);
        }

        [HttpGet]
        public IActionResult PartnerDashboardPickups()
        {
            DashboardViewModel model = new DashboardViewModel();
            PostBusiness postBU = new PostBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();

            model.Partner = new Partner();
            model.PickupPoints = new List<PickupPointDetails>();
            model.UncollectedPostStorageDetails = new List<PostStorageDetails>();
            model.Partners = partnerBU.GetPartners();

            return View(model);
        }

        [HttpPost]
        public IActionResult PartnerDashboardPickups(DashboardViewModel model, Partner partner)
        {
            PostBusiness postBU = new PostBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();
            PickupPointBusiness ppBU = new PickupPointBusiness();
            StorageBusiness storageBU = new StorageBusiness();

            model.Partners = partnerBU.GetPartners();
            model.PickupPoints = ppBU.GetPickupPointsByIdPartner(partner.Id);
            model.UncollectedPostStorageDetails = postBU.GetPostStorageDetailsAwaitingPickupByPId(partner.Id);

            return View(model);
        }

        [HttpPost]
        public IActionResult PickupPtConfirmation(int postID)
        {   
            MailNotificationBusiness mailBU = new MailNotificationBusiness();
            PostBusiness postBusiness = new PostBusiness();
            Post post = postBusiness.GetPostById(postID);

            if (post.DepositDate == null || post.DepositDate == DateTime.MinValue)
            {
                post.DepositDate = DateTime.Now;
                postBusiness.Update(post);
                
                PostStorageDetails postStorageDetails = postBusiness.GetPostStorageDetailsByID(postID);
                SubscriberBusiness subBU = new SubscriberBusiness();
                Subscriber recipient = subBU.GetSubscriberById(postStorageDetails.IdReceivingSubscriber);
                mailBU.NewDepositMail(recipient, postStorageDetails);
            }
            else
            {
                post.CollectionDate = DateTime.Now;
                postBusiness.Update(post);

                PostStorageDetails postStorageDetails = postBusiness.GetPostStorageDetailsByID(postID);
                SubscriberBusiness subBU = new SubscriberBusiness();
                Subscriber donator = subBU.GetSubscriberById(postStorageDetails.IdDonatingSubscriber);
                mailBU.NewCollectionMail(donator, postStorageDetails);
            }

            return View("PickupPtConfirmation", "Home");
        }


    }
}