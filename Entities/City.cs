﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class City
    {
        public int Id { get; set; }
        public string ZipCode { get; set; }
        public string CityName { get; set; }

    }
}
