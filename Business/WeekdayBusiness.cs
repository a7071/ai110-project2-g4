﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class WeekdayBusiness
    {
        public List<Weekday> GetWeekdays()
        {
            WeekdayDAO dao = new WeekdayDAO();

            return dao.GetAll();
        }


        public Weekday GetDayById(int id)
        {
            WeekdayDAO dao = new WeekdayDAO();
            return dao.GetById(id);
        }


    }
}
