﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class DisplayController : Controller
    {
        public IActionResult Index()
        {
            PostViewModel model = new PostViewModel();

            PostBusiness postBu = new PostBusiness();
            model.Post = postBu.GetPostById(7);

            ProductLineBusiness productLineBu = new ProductLineBusiness();
            model.ProductLine = productLineBu.GetProductLine(7);

            UnitBusiness unitBusiness = new UnitBusiness();
            model.Unit = unitBusiness.GetUnit(model.ProductLine.IdUnit);

            ProductBusiness productBusiness = new ProductBusiness();
            model.Product = productBusiness.GetProduct(model.ProductLine.IdProduct);
            
            ProductTypeBusiness productTypeBusiness = new ProductTypeBusiness();
            model.ProductType = productTypeBusiness.GetProductType(model.Product.IdProductType);

            SubCategoryBusiness subCategoryBusiness = new SubCategoryBusiness();
            model.SubCategory = subCategoryBusiness.GetSubCategory(model.ProductType.IdSubCategory);

            CategoryBusiness categoryBusiness = new CategoryBusiness();
            model.Category = categoryBusiness.GetCategory(model.SubCategory.IdCategory);

            CityBusiness cityBusiness = new CityBusiness();
            model.City = cityBusiness.GetCityById(model.Post.IdCityOfDeposit);

            StorageBusiness storageBusiness = new StorageBusiness();
            model.Storage = storageBusiness.GetStorage(model.Post.IdStorage);

            VolumeBusiness volumeBusiness = new VolumeBusiness();
            model.Volume = volumeBusiness.GetVolume(model.Storage.IdVolume);

            StorageTypeBusiness storageTypeBusiness = new StorageTypeBusiness();
            model.StorageType = storageTypeBusiness.GetStorageType(model.Storage.IdStorageType);

            PickupPointBusiness pickupPointBusiness = new PickupPointBusiness();
            model.PickupPoint = pickupPointBusiness.GetPickupPoint(model.Storage.IdPickupPoint);

            return View(model);
        }
        [HttpPost]
        public IActionResult Index(PostViewModel model)
        {
            return RedirectToAction("Index", "Home");
        }

        [HttpPost]
        public IActionResult Delete(PostViewModel model)
        {  
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(7);
            post.CancellationDate = DateTime.Now;
            postBu.Update(post);
            return View();
        }
    }
}
