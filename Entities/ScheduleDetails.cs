﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class ScheduleDetails : Schedule
    {
        public string DayName { get; set; }

        public ScheduleDetails(Schedule schedule)
        {
            Id = schedule.Id;
            IdDay = schedule.IdDay;
            IdPickupPoint = schedule.IdPickupPoint;
            OpeningHours = schedule.OpeningHours;
            ClosingHours = schedule.ClosingHours;
        }

        public ScheduleDetails(int idDay, int idPickup, string openingHours, string closingHours, string dayName)
        {
            IdDay = idDay;
            IdPickupPoint = idPickup;
            OpeningHours = openingHours;
            ClosingHours = closingHours;
            DayName = dayName;
        }

        public ScheduleDetails()
        {
        }
    }
}
