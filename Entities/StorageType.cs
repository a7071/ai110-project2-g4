﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class StorageType
    {
        public int Id { get; set; }
        public string Type { get; set; }

    }
}
