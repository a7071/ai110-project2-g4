﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class RatingDAO : DAO
    {
        public void Create(Rating rating)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO notation
                                (idAdherent,Beneficiaire_idAdherent,idAnnonce,note,commentaire,dateNote)
                                VALUES
                                (@idSubscriber,@idReceivingSub,@idPost,@score,@comment,Date(SYSDATE()))";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", rating.IdSubscriber));
            cmd.Parameters.Add(new MySqlParameter("idReceivingSub", rating.IdReceivingSub));
            cmd.Parameters.Add(new MySqlParameter("idPost", rating.IdPost));
            cmd.Parameters.Add(new MySqlParameter("score", rating.Score));
            cmd.Parameters.Add(new MySqlParameter("comment", rating.Comment));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion d'une note" + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<Rating> GetRatings(int idSubscriber)
        {
            List<Rating> result = new List<Rating>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM notation WHERE (Beneficiaire_idAdherent = @id OR @id = 0)";

            cmd.Parameters.Add(new MySqlParameter("id", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Rating rating = DataReaderToEntity(dr);
                    result.Add(rating);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des notes et commentaires" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Rating GetRatingByIdSubOfIdPost(int idRatingSub, int idPost)
        {
            Rating result = new Rating();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM notation WHERE (Beneficiaire_idAdherent = @idSub)
                                                       AND (idAnnonce = @idPost)";

            cmd.Parameters.Add(new MySqlParameter("idSub", idRatingSub));
            cmd.Parameters.Add(new MySqlParameter("idPost", idPost));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération de la note du Donateur sur le Bénéficiaire" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Rating DataReaderToEntity(DbDataReader dr)
        {
            Rating result = new Rating();
            result.Id = dr.GetInt32(dr.GetOrdinal("idNotation"));
            result.IdSubscriber = dr.GetInt32(dr.GetOrdinal("idAdherent"));
            result.IdReceivingSub = dr.GetInt32(dr.GetOrdinal("Beneficiaire_idAdherent"));
            result.IdPost = dr.GetInt32(dr.GetOrdinal("idAnnonce"));
            result.Score = dr.GetInt32(dr.GetOrdinal("note"));
            result.Comment = dr.GetString(dr.GetOrdinal("commentaire"));
            result.ScoreDate = dr.GetDateTime(dr.GetOrdinal("dateNote"));
            return result;
        }
    }
}
