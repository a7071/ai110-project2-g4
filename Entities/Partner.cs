﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Partner
    {
        public int Id { get; set; }
        public int IdCity { get; set; }
        public string PartnerLogin { get; set; }
        public string PartnerPassword { get; set; }
        public string RepresentativeLastName { get; set; }
        public string RepresentativeName { get; set; }
        public string RepresentativePhone { get; set; }
        public string RepresentativeMail { get; set; }
        public string InfrastructureName { get; set; }
        public string SiretNumber { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime UnSubscriptionDate { get; set; }
        public int PinCode { get; set; }
    }
}
