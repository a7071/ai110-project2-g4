﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using WebApp.Models;
using Microsoft.AspNetCore.Mvc;


namespace Fr.EQL.AI110.DLD_GD.WebApp.Controllers
{
    public class PostController : Controller
    {
        public const int DEFAULT_DISTANCE = 50;
        public const int DEFAULT_CITY = 1;

        [HttpGet]
        public IActionResult Index()
        {
            PostSearchViewModel model = new PostSearchViewModel();

            PostBusiness postBU = new PostBusiness();
            model.Posts = postBU.GetPosts();

            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            CategoryBusiness categoryBU = new CategoryBusiness();
            model.Categories = categoryBU.GetCategories();

            ProductLineBusiness productLineBU = new ProductLineBusiness();

            foreach (PostDetails post in model.Posts)
            {
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                post.CityZipCode = cityBU.GetCityById(post.IdCityOfDeposit).ZipCode;
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                }
                post.ProductLineDetails = productLineBU.GetProductLineDetails(post.Id);
            }

            model.Posts = postBU.OrderByPostDate(model.Posts, model.DateOrder);

            return View(model);
        }


        [HttpPost]
        public IActionResult Index(PostSearchViewModel model)
        {
            PostBusiness postBU = new PostBusiness();
            int idSearchedCity = DEFAULT_CITY;

            if (model.IdSearchedCity != 0)
            {
                idSearchedCity = model.IdSearchedCity;
            }
            else
            {
                model.Distance = DEFAULT_DISTANCE;
            }
            
            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            CategoryBusiness categoryBU = new CategoryBusiness();
            model.Categories = categoryBU.GetCategories();

            model.Posts = postBU.SearchPosts(model.Title, idSearchedCity, model.Distance, model.IdDonatingSubscriber, model.DateOrder, model.IdSearchedCategory);

            return View(model);
        }

        [HttpGet]
        [Route("Post/MyPosts/{userId}")]
        public IActionResult MyPosts(int userId)
        {
            PostSearchViewModel model = new PostSearchViewModel();

            PostBusiness postBU = new PostBusiness();

            model.Posts = postBU.GetPostsByIdSubscriber(userId);

            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            foreach (PostDetails post in model.Posts)
            {
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
            }

            return View(model);
        }

        public IActionResult DisplayPost(int id)
        {
            PostViewModel model = new PostViewModel();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            PostBusiness postBu = new PostBusiness();
            model.Post = postBu.GetPostById(id);

            ProductLineBusiness productLineBu = new ProductLineBusiness();
            model.ProductLine = productLineBu.GetProductLine(id);

            UnitBusiness unitBu = new UnitBusiness();
            model.Unit = unitBu.GetUnit(model.ProductLine.IdUnit);

            if (model.Post.IdStorage != 0)
            {
            StorageBusiness storageBusiness = new StorageBusiness();
            model.Storage = storageBusiness.GetStorage(model.Post.IdStorage);

            PickupPointBusiness pickupPointBusiness = new PickupPointBusiness();
            model.PickupPoint = pickupPointBusiness.GetPickupPoint(model.Storage.IdPickupPoint);
            }

            CityBusiness cityBusiness = new CityBusiness();
            model.City = cityBusiness.GetCityById(model.Post.IdCityOfDeposit);

            return View(model);
        }

        [HttpPost]
        public IActionResult BookPost(PostViewModel model)
        {

          PostBusiness postBusiness = new PostBusiness();
          Post post = postBusiness.GetPostById(model.Post.Id);
          post.IdReceivingSubscriber = model.Subscriber.Id;
          post.ReservationDate = DateTime.Now;
          postBusiness.Update(post);
          MailNotificationBusiness mailBU = new MailNotificationBusiness();

          SubscriberBusiness subBU = new SubscriberBusiness();
          Subscriber donator = subBU.GetSubscriberById(post.IdDonatingSubscriber);
          mailBU.NewReservationMail(donator);

          return View(model);
        }

       
    }
}
