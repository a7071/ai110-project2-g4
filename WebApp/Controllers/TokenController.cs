﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class TokenController : Controller
    {
        public IActionResult Index()
        {
            TokenViewModel model = new TokenViewModel();

            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscribers = subBu.GetSubscribers();

            return View(model);
        }

        [HttpPost]
        public IActionResult Index(TokenViewModel model)
        {

            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscribers = subBu.GetSubscribers();

            PostBusiness postBu = new PostBusiness();
            model.Donation = postBu.GetPastDonations(model.Subscriber.Id).Count;
            model.Received = postBu.GetPastReservations(model.Subscriber.Id).Count;

            PurchaseTokenBusiness purchaseBu = new PurchaseTokenBusiness();
            model.Amount = purchaseBu.GetAmountsOfIdSub(model.Subscriber.Id) + model.Donation/2 - model.Received;



            return View(model);
        }
        [HttpPost]
        public IActionResult BuyToken(TokenViewModel model)
        {
            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscriber = subBu.GetSubscriberById(model.Subscriber.Id);

            PurchaseTokenBusiness purchaseBu = new PurchaseTokenBusiness();
            model.Amount = purchaseBu.GetAmountsOfIdSub(model.Subscriber.Id);

            TokenBundleBusiness bundleBu = new TokenBundleBusiness();
            model.Bundles = bundleBu.GetTokenBundles();

            return View(model);
            
        }
        public IActionResult Calculate(TokenViewModel model)
        {
            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscriber = subBu.GetSubscriberById(model.Subscriber.Id);

            PurchaseTokenBusiness purchaseBu = new PurchaseTokenBusiness();
            model.Amount = purchaseBu.GetAmountsOfIdSub(model.Subscriber.Id);

            TokenBundleBusiness bundleBu = new TokenBundleBusiness();
            if (Request.Form["token"] != "")
            {
                int bundle = int.Parse(Request.Form["token"]);
                float price = bundleBu.GetTokenBundle(bundle).BundlePrice;
                model.Calculate = price * model.PurchaseLine.Quantity;
            }

            model.Bundles = bundleBu.GetTokenBundles();

            return View("BuyToken",model);
        }
        public IActionResult Purchase(TokenViewModel model)
        {
            SubscriberBusiness subBu = new SubscriberBusiness();
            //model.Subscribers = subBu.GetSubscribers();
            model.Subscriber = subBu.GetSubscriberById(model.Subscriber.Id);

            TokenBundleBusiness tokenBu = new TokenBundleBusiness();
            model.Bundles = tokenBu.GetTokenBundles();
            int bundle = int.Parse(Request.Form["token"]);

            PurchaseTokenBusiness tokenPurchaseBu = new PurchaseTokenBusiness();
            TokenPurchase token = new TokenPurchase();
            token.IdSubscriber = model.Subscriber.Id;
            if(model.PurchaseLine.Quantity == 0)
            {
                model.PurchaseLine.Quantity = 1;
            }
            token.TokensNumber = tokenBu.GetTokenBundle(bundle).BundleAmount*model.PurchaseLine.Quantity;
            token.PurchaseAmount = tokenBu.GetTokenBundle(bundle).BundlePrice*model.PurchaseLine.Quantity;
            tokenPurchaseBu.SaveToken(token);

            PurchaseLineBusiness lineBu = new PurchaseLineBusiness();
            PurchaseLine line = new PurchaseLine();
            line.IdTokenPurchase = token.Id;
            line.IdTokenBundle = bundle;
            line.Quantity = model.PurchaseLine.Quantity;
            lineBu.SavePurchaseLine(line);

            model.Message = "Votre achat de crédit a bien été pris en compte";

            return View("BuyToken",model);
        }
    }
}
