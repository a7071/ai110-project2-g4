﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class CityBusiness
    {
        public List<City> GetCities()
        {
            CityDAO cityDAO = new CityDAO();
            return cityDAO.GetAll();
        }

        public City GetCityById(int id)
        {
            CityDAO cityDAO = new CityDAO();
            return cityDAO.GetCity(id);
        }


        
    }
}
