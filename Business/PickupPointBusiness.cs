using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PickupPointBusiness
    {
        public void SavePickupPoint(PickupPoint pickup)
        {

            PickupPointDAO dao = new PickupPointDAO();

            if (pickup.Id == 0)
            {
                dao.InsertPickupPoint(pickup);
            }
            else
            {
                dao.Update(pickup);
            }
        }


        public void DesactivatePickupPoint(PickupPoint pickup)
        {
            pickup.DeletionDate = DateTime.Now;
            PickupPointDAO dao = new PickupPointDAO();
            dao.Update(pickup);
        }


        public void UpdatePickupPoint(PickupPoint pickup)
        {
            PickupPointDAO dao = new PickupPointDAO();
            dao.Update(pickup);
        }

        public DateTime FindCurrentTime()
        {
            DateTime now = DateTime.Now;
            return now;
        }

       public PickupPointDetails GetPickupDetailsById(int id)
        {
            PickupPointDAO dao = new PickupPointDAO();
            PickupPointDetails ppDetails = dao.GetPickupDetails(id);

            WeekdayBusiness weekdayBU = new WeekdayBusiness();
            CityBusiness cityBU = new CityBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();
            ScheduleBusiness scheduleBU = new ScheduleBusiness();

            ppDetails.ZipCode = cityBU.GetCityById(ppDetails.IdCity).ZipCode;
            ppDetails.CityName = cityBU.GetCityById(ppDetails.IdCity).CityName;
            ppDetails.ScheduleDetailsList = scheduleBU.GetSchedulesByIdPickup(ppDetails.Id);
            ppDetails.PickupPartner = partnerBU.GetPartnerById(ppDetails.IdPartner);

            foreach (ScheduleDetails scheDetails in ppDetails.ScheduleDetailsList)
            {

                scheDetails.DayName = weekdayBU.GetDayById(scheDetails.IdDay).Day;
            }
            return ppDetails;
        }

        public List<PickupPointDetails> GetPickupPointsByIdPartner(int id)
        {
            List<PickupPointDetails> pickupDetailsList = new List<PickupPointDetails>();
            PartnerBusiness partnerBU = new PartnerBusiness();
            StorageBusiness storageBU = new StorageBusiness();
            PickupPointDAO dao = new PickupPointDAO();
            CityBusiness cityBU = new CityBusiness();

            pickupDetailsList = dao.GetByIdPartner(id);
            foreach (PickupPointDetails pickup in pickupDetailsList)
            {
                pickup.PickupPartner = partnerBU.GetPartnerById(pickup.IdPartner);
                pickup.Storages = storageBU.ShowStorageDetails(pickup.Id);
                pickup.ZipCode = cityBU.GetCityById(pickup.IdCity).ZipCode;
                pickup.CityName = cityBU.GetCityById(pickup.IdCity).CityName;
            }
            return pickupDetailsList;
        }

        public List<PickupPointDetails> GetAllPickupDetails()
        {
            PickupPointDAO dao = new PickupPointDAO();
            List <PickupPointDetails> pickupDetailsList = dao.GetAllPickupDetails();

            WeekdayBusiness weekdayBU = new WeekdayBusiness();
            CityBusiness cityBU = new CityBusiness();
            ScheduleBusiness scheduleBU = new ScheduleBusiness();
            PartnerBusiness partnerBU = new PartnerBusiness();
            StorageBusiness storageBU = new StorageBusiness();

            foreach (PickupPointDetails ppDetails in pickupDetailsList)
            {
                ppDetails.ZipCode = cityBU.GetCityById(ppDetails.IdCity).ZipCode;
                ppDetails.CityName = cityBU.GetCityById(ppDetails.IdCity).CityName;
                ppDetails.PickupPartner = partnerBU.GetPartnerById(ppDetails.IdPartner);
                ppDetails.ScheduleDetailsList = scheduleBU.GetSchedulesByIdPickup(ppDetails.Id);
                ppDetails.Storages = storageBU.ShowStorageDetails(ppDetails.Id);    

                foreach (ScheduleDetails scheDetails in ppDetails.ScheduleDetailsList)
                {
                    
                    scheDetails.DayName = weekdayBU.GetDayById(scheDetails.IdDay).Day;
                }
            }
            return pickupDetailsList;
        }

        public PickupPoint GetPickupPoint(int id)
        {
            PickupPointDAO dao = new PickupPointDAO();
            return dao.GetPickupPoint(id);
        }

        public List<PickupPoint> GetPickupPointsByMultiCriteria(int idCity, int idVolume, int idType)
        {
            PickupPointDAO dao = new PickupPointDAO();
            return dao.GetPickupPointsByMultiCriteria(idCity,idVolume,idType);
        }
    }
}
