﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class WeekdayDAO : DAO
    {
        private Weekday DataReaderToEntity(DbDataReader dr)
        {
            Weekday day = new Weekday();

            day.Id = dr.GetInt32(dr.GetOrdinal("idJour"));
            day.Day = dr.GetString(dr.GetOrdinal("jour"));

            return day;
        }

        public List<Weekday> GetAll()
        {
            List<Weekday> weekdays = new List<Weekday>();

            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = "SELECT * FROM jourdelasemaine";

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Weekday day = DataReaderToEntity(dr);
                    weekdays.Add(day);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de lecture de la base de données." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return weekdays;
        }


        public Weekday GetById(int id)
        {
            Weekday result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM jourdelasemaine WHERE idJour = @idDay";
            cmd.Parameters.Add(new MySqlParameter("idDay", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un jour de la semaine : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
    }
}
