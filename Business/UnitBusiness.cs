﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class UnitBusiness
    {
        public List<Unit> GetUnits()
        {
            UnitDAO dao = new UnitDAO();
            return dao.GetUnits();
        }

        public List<Unit> GetUnitsByIds(int idProduct)
        {
            List<Unit> result = new List<Unit>();

            UnitDAO dao = new UnitDAO();
            dao.GetUnitsByProduct(idProduct);

            foreach (int id in dao.GetUnitsByProduct(idProduct))
            {
                result.Add(dao.GetUnit(id));
            }

            return result;

        }

        public Unit GetUnit(int id)
        {
            UnitDAO dao = new UnitDAO();
            return dao.GetUnit(id);
        }
    }
}
