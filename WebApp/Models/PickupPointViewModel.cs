﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class PickupPointViewModel
    {
        //public Schedule Schedule { get; set; }

        public PickupPoint Pickup { get; set; }

        public int IdPartner { get; set; } = 0;

        public List<Weekday> Days { get; set; }

        public List<City> CityList { get; set; }

        
        public Dictionary<string, Schedule> Planning { get; set; } = new Dictionary<string, Schedule>();
        
        public List<PickupPointDetails> PickupPoints { get; set; }
        
        public List<Partner> Partners { get; set; }

        public PickupPointDetails PickupDetails { get; set; }

    }
}
