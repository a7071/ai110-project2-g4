/*==============================================================*/
/* Nom de SGBD :  MySQL 5.0                                     */
/* Date de création :  13/12/2021 11:07:46                      */
/*==============================================================*/


drop table if exists AchatDeCredit;

drop table if exists Adherent;

drop table if exists Annonce;

drop table if exists Categorie;

drop table if exists Distance;

drop table if exists Emplacement;

drop table if exists Fonction;

drop table if exists Forfait;

drop table if exists HorairesD_ouverture;

drop table if exists Indisponibilite;

drop table if exists JourDeLaSemaine;

drop table if exists LigneAchat;

drop table if exists LigneProduit;

drop table if exists Notation;

drop table if exists Partenaire;

drop table if exists PointDeDepot;

drop table if exists Produit;

drop table if exists SousCategorie;

drop table if exists TypeD_emplacement;

drop table if exists TypeDeProduit;

drop table if exists Unite;

drop table if exists Ville;

drop table if exists Volume;

drop table if exists mesurer;

drop table if exists qualifier;

/*==============================================================*/
/* Table : AchatDeCredit                                        */
/*==============================================================*/
create table AchatDeCredit
(
   idAchatCredit        int not null auto_increment,
   idAdherent           int not null,
   dateAchat            datetime,
   nombreCredits        int,
   montantAchat         float,
   primary key (idAchatCredit)
);

/*==============================================================*/
/* Table : Adherent                                             */
/*==============================================================*/
create table Adherent
(
   idAdherent           int not null auto_increment,
   idFonction           int,
   idVille              int not null,
   adherent             varchar(254),
   motDePasse           varchar(254),
   nom                  varchar(254),
   prenom               varchar(254),
   dateDeNaissance      datetime,
   numRue               varchar(254),
   nomRue               varchar(254),
   telephone            varchar(254),
   mail                 varchar(254),
   dateInscription      datetime,
   dateActivation       datetime,
   dateDesactivation    datetime,
   codeVerification     int,
   primary key (idAdherent)
);

/*==============================================================*/
/* Table : Annonce                                              */
/*==============================================================*/
create table Annonce
(
   idAnnonce            int not null auto_increment,
   idAdherent           int,
   idEmplacement        int,
   Beneficiaire_idAdherent int,
   titre                varchar(254),
   description          varchar(254),
   photo				varchar(254),
   datePublication      datetime,
   dateReservation      datetime,
   dateValidation		datetime,
   dateAnnulation       datetime,
   dateRetrait          datetime,
   dateDepot            datetime,
   dateCollecte         datetime,
   codeEchange          int,
   idVilleDepot         int,
   primary key (idAnnonce)
);

/*==============================================================*/
/* Table : Categorie                                            */
/*==============================================================*/
create table Categorie
(
   idCategorie          int not null auto_increment,
   categorie            varchar(254),
   primary key (idCategorie)
);

/*==============================================================*/
/* Table : Distance                                             */
/*==============================================================*/
create table Distance
(
   idVille              int not null,
   Ville_idVille        int not null,
   distanceMoyenne      int,
   primary key (idVille, Ville_idVille)
);

/*==============================================================*/
/* Table : Emplacement                                          */
/*==============================================================*/
create table Emplacement
(
   idEmplacement        int not null auto_increment,
   idVolume             int,
   idTypeEmplacement    int,
   idPointDepot         int,
   dateAjout            datetime,
   dateRetrait          datetime,
   dateAnnulation       datetime,
   primary key (idEmplacement)
);

/*==============================================================*/
/* Table : Fonction                                             */
/*==============================================================*/
create table Fonction
(
   idFonction           int not null auto_increment,
   profil               varchar(254),
   primary key (idFonction)
);

/*==============================================================*/
/* Table : Forfait                                              */
/*==============================================================*/
create table Forfait
(
   idForfait            int not null auto_increment,
   nomForfait           varchar(254),
   nombreCreditsForfait int,
   prixForfait          float,
   primary key (idForfait)
);

/*==============================================================*/
/* Table : HorairesD_ouverture                                  */
/*==============================================================*/
create table HorairesD_ouverture
(
   idHoraires           int not null auto_increment,
   idJour               int,
   idPointDepot         int,
   heureD_ouverture     varchar(254),
   heureFermeture       varchar(254),
   primary key (idHoraires)
);

/*==============================================================*/
/* Table : Indisponibilite                                      */
/*==============================================================*/
create table Indisponibilite
(
   idIndisponibilite    int not null auto_increment,
   idPartenaire         int,
   idAdherent           int,
   debutIndisponibilite datetime,
   finIndisponibilite   datetime,
   primary key (idIndisponibilite)
);

/*==============================================================*/
/* Table : JourDeLaSemaine                                      */
/*==============================================================*/
create table JourDeLaSemaine
(
   idJour               int not null auto_increment,
   jour                 varchar(254),
   primary key (idJour)
);

/*==============================================================*/
/* Table : LigneAchat                                           */
/*==============================================================*/
create table LigneAchat
(
   idAchatCredit        int not null,
   idForfait            int not null,
   quantite             int,
   primary key (idAchatCredit, idForfait)
);

/*==============================================================*/
/* Table : LigneProduit                                         */
/*==============================================================*/
create table LigneProduit
(
   idAnnonce            int not null,
   idProduit            int not null,
   idUnite              int not null,
   quantite             float,
   dlc                  datetime,
   primary key (idAnnonce, idProduit)
);

/*==============================================================*/
/* Table : Notation                                             */
/*==============================================================*/
create table Notation
(
   idNotation           int not null auto_increment,
   idAdherent           int,
   Beneficiaire_idAdherent int,
   idAnnonce            int,
   note                 int,
   commentaire          varchar(254),
   dateNote             datetime,
   primary key (idNotation)
);

/*==============================================================*/
/* Table : Partenaire                                           */
/*==============================================================*/
create table Partenaire
(
   idPartenaire         int not null auto_increment,
   idVille              int not null,
   partenaire           varchar(254),
   motDePasse           varchar(254),
   nomRepresentant      varchar(254),
   prenomRepresentant   varchar(254),
   telephoneRepresentant varchar(254),
   mailRepresentant     varchar(254),
   nomInfrastructure    varchar(254),
   numSiret             varchar(254),
   numRue               varchar(254),
   nomRue               varchar(254),
   telephone            varchar(254),
   email                varchar(254),
   dateInscription      datetime,
   dateActivation       datetime,
   dateDesactivation    datetime,
   codeVerification     int,
   primary key (idPartenaire)
);

/*==============================================================*/
/* Table : PointDeDepot                                         */
/*==============================================================*/
create table PointDeDepot
(
   idPointDepot         int not null auto_increment,
   idPartenaire         int not null,
   idVille              int,
   nomPointDepot        varchar(254),
   numRue               varchar(254),
   nomRue               varchar(254),
   dateCreation         datetime,
   dateRetrait          datetime,
   primary key (idPointDepot)
);

/*==============================================================*/
/* Table : Produit                                              */
/*==============================================================*/
create table Produit
(
   idProduit            int not null auto_increment,
   idTypeProduit        int,
   nomProduit           varchar(254),
   primary key (idProduit)
);

/*==============================================================*/
/* Table : SousCategorie                                        */
/*==============================================================*/
create table SousCategorie
(
   idSousCategorie      int not null auto_increment,
   idCategorie          int,
   sousCategorie        varchar(254),
   primary key (idSousCategorie)
);

/*==============================================================*/
/* Table : TypeD_emplacement                                    */
/*==============================================================*/
create table TypeD_emplacement
(
   idTypeEmplacement    int not null auto_increment,
   typeEmplacement      varchar(254),
   primary key (idTypeEmplacement)
);

/*==============================================================*/
/* Table : TypeDeProduit                                        */
/*==============================================================*/
create table TypeDeProduit
(
   idTypeProduit        int not null auto_increment,
   idSousCategorie      int,
   typeProduit          varchar(254),
   primary key (idTypeProduit)
);

/*==============================================================*/
/* Table : Unite                                                */
/*==============================================================*/
create table Unite
(
   idUnite              int not null auto_increment,
   unite                varchar(254),
   primary key (idUnite)
);

/*==============================================================*/
/* Table : Ville                                                */
/*==============================================================*/
create table Ville
(
   idVille              int not null auto_increment,
   codePostal           varchar(254),
   ville                varchar(254),
   primary key (idVille)
);

/*==============================================================*/
/* Table : Volume                                               */
/*==============================================================*/
create table Volume
(
   idVolume             int not null auto_increment,
   format               varchar(254),
   hauteur              int,
   largeur              int,
   longueur             int,
   primary key (idVolume)
);

/*==============================================================*/
/* Table : mesurer                                              */
/*==============================================================*/
create table mesurer
(
   idProduit            int not null,
   idUnite              int not null,
   primary key (idProduit, idUnite)
);

/*==============================================================*/
/* Table : qualifier                                            */
/*==============================================================*/
create table qualifier
(
   idTypeEmplacement    int not null,
   idCategorie          int not null,
   primary key (idTypeEmplacement, idCategorie)
);

alter table AchatDeCredit add constraint FK_association3 foreign key (idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;

alter table Adherent add constraint FK_association12 foreign key (idVille)
      references Ville (idVille) on delete restrict on update restrict;

alter table Adherent add constraint FK_association24 foreign key (idFonction)
      references Fonction (idFonction) on delete restrict on update restrict;

alter table Annonce add constraint FK_association9 foreign key (idEmplacement)
      references Emplacement (idEmplacement) on delete restrict on update restrict;

alter table Annonce add constraint FK_associationPublier17 foreign key (idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;

alter table Annonce add constraint FK_associationReserver26 foreign key (Beneficiaire_idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;

alter table Distance add constraint FK_association41 foreign key (idVille)
      references Ville (idVille) on delete restrict on update restrict;

alter table Distance add constraint FK_association42 foreign key (Ville_idVille)
      references Ville (idVille) on delete restrict on update restrict;

alter table Emplacement add constraint FK_association18 foreign key (idPointDepot)
      references PointDeDepot (idPointDepot) on delete restrict on update restrict;

alter table Emplacement add constraint FK_association25 foreign key (idVolume)
      references Volume (idVolume) on delete restrict on update restrict;

alter table Emplacement add constraint FK_association28 foreign key (idTypeEmplacement)
      references TypeD_emplacement (idTypeEmplacement) on delete restrict on update restrict;

alter table HorairesD_ouverture add constraint FK_association20 foreign key (idPointDepot)
      references PointDeDepot (idPointDepot) on delete restrict on update restrict;

alter table HorairesD_ouverture add constraint FK_association39_Jour foreign key (idJour)
      references JourDeLaSemaine (idJour) on delete restrict on update restrict;

alter table Indisponibilite add constraint FK_association22 foreign key (idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;

alter table Indisponibilite add constraint FK_association32 foreign key (idPartenaire)
      references Partenaire (idPartenaire) on delete restrict on update restrict;

alter table LigneAchat add constraint FK_association_achat foreign key (idAchatCredit)
      references AchatDeCredit (idAchatCredit) on delete restrict on update restrict;

alter table LigneAchat add constraint FK_association_forfait foreign key (idForfait)
      references Forfait (idForfait) on delete restrict on update restrict;

alter table LigneProduit add constraint FK_association34 foreign key (idUnite)
      references Unite (idUnite) on delete restrict on update restrict;

alter table LigneProduit add constraint FK_association_annonce foreign key (idAnnonce)
      references Annonce (idAnnonce) on delete restrict on update restrict;

alter table LigneProduit add constraint FK_association_produit foreign key (idProduit)
      references Produit (idProduit) on delete restrict on update restrict;

alter table Notation add constraint FK_association27 foreign key (idAnnonce)
      references Annonce (idAnnonce) on delete restrict on update restrict;

alter table Notation add constraint FK_association29 foreign key (idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;
	  
alter table Notation add constraint FK_association_beneficiairee foreign key (Beneficiaire_idAdherent)
      references Adherent (idAdherent) on delete restrict on update restrict;

alter table Partenaire add constraint FK_association15 foreign key (idVille)
      references Ville (idVille) on delete restrict on update restrict;

alter table PointDeDepot add constraint FK_association31 foreign key (idVille)
      references Ville (idVille) on delete restrict on update restrict;

alter table PointDeDepot add constraint FK_association8 foreign key (idPartenaire)
      references Partenaire (idPartenaire) on delete restrict on update restrict;

alter table Produit add constraint FK_association36 foreign key (idTypeProduit)
      references TypeDeProduit (idTypeProduit) on delete restrict on update restrict;

alter table SousCategorie add constraint FK_association38 foreign key (idCategorie)
      references Categorie (idCategorie) on delete restrict on update restrict;

alter table TypeDeProduit add constraint FK_association35 foreign key (idSousCategorie)
      references SousCategorie (idSousCategorie) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurer_produit foreign key (idProduit)
      references Produit (idProduit) on delete restrict on update restrict;

alter table mesurer add constraint FK_mesurer_unite foreign key (idUnite)
      references Unite (idUnite) on delete restrict on update restrict;

alter table qualifier add constraint FK_qualifier_categorie foreign key (idCategorie)
      references Categorie (idCategorie) on delete restrict on update restrict;

alter table qualifier add constraint FK_qualifier_emplacement foreign key (idTypeEmplacement)
      references TypeD_emplacement (idTypeEmplacement) on delete restrict on update restrict;

