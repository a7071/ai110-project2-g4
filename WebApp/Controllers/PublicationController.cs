﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class PublicationController : Controller
    {
        public IActionResult AIndex()
        {
            PostViewModel model = new PostViewModel();

            //Liste des catégories
            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            CategoryBusiness categoryBu = new CategoryBusiness();
            model.Categories = categoryBu.GetCategories();

            return View(model);
        }
        [HttpPost]
        public IActionResult BCategory(PostViewModel model)
        {
            GetNameFromBu(model);

            //Liste sous-catégories
            SubCategoryBusiness subCategoryBu = new SubCategoryBusiness();
            model.SubCategories = subCategoryBu.GetSubCategoriesByCategory(model.Category.Id);

            return View(model);
        }

        [HttpPost]
        public IActionResult CSubCategory(PostViewModel model)
        {
            GetNameFromBu(model);

            //Liste Types de produit
            ProductTypeBusiness productTypeBu = new ProductTypeBusiness();
            model.ProductTypes = productTypeBu.GetProductTypesBySubCategories(model.SubCategory.Id);

            return View(model);
        }

        [HttpPost]
        public IActionResult DProductType(PostViewModel model)
        {
            GetNameFromBu(model);

            //Liste des produits
            ProductBusiness productBu = new ProductBusiness();
            model.Products = productBu.GetProductsByProductType(model.ProductType.Id);

            return View(model);
        }
        [HttpPost]
        public IActionResult EValider(PostViewModel model)
        {
            GetNameFromBu(model);
            UnitBusiness unitBu = new UnitBusiness();
            model.Units = unitBu.GetUnitsByIds(model.Product.Id);

           return View(model);
   
        }

        [HttpPost]
        public async Task<IActionResult> Fproduct(PostViewModel model)
        {
            GetNameFromBu(model);

            IFormFile file = model.MyFile;

            if (file != null)
            {
                var path = Path.Combine(
                            Directory.GetCurrentDirectory(), "wwwroot\\images\\upload",
                            file.FileName);

                model.Post.Picture = file.FileName;

                using (var stream = new FileStream(path, FileMode.Create))
                {
                    await file.CopyToAsync(stream);
                }
            }
            
            if (Request.Form["mode"] == "home")
            {
                return View("ghome",model);
            }
            else
            {
                VolumeBusiness volumeBu = new VolumeBusiness();
                model.Volumes = volumeBu.GetVolumes();

                CityBusiness cityBu = new CityBusiness();
                model.Cities = cityBu.GetCities();

                StorageTypeBusiness storageTypeBu = new StorageTypeBusiness();
                model.StorageTypes = storageTypeBu.GetStorageTypesByIdCategories(model.Category.Id);

                return View("gformat",model);
            }
           
        }

        [HttpPost]
        public IActionResult HPublishHome(PostViewModel model)
        {
            GetNameFromBu(model);
     
            PostBusiness postBu = new PostBusiness();
            Post post = new Post();
            post.IdDonatingSubscriber = model.Subscriber.Id;
            post.IdStorage = 0;
            post.Title = model.Post.Title;
            post.Description = model.Post.Description;
            post.Picture = model.Post.Picture;
            post.IdCityOfDeposit = model.Subscriber.IdCity;
            postBu.SavePost(post);

            ProductLineBusiness productLineBu = new ProductLineBusiness();
            ProductLine productLine = new ProductLine();
            productLine.IdPost = post.Id;
            productLine.IdProduct = model.Product.Id;
            productLine.IdUnit = model.Unit.Id;
            productLine.Quantity = model.ProductLine.Quantity;
            productLine.BestBeforeDate = model.ProductLine.BestBeforeDate;
            productLineBu.CreateProductLine(productLine);

            return View(model);
        }

        [HttpPost]
        public IActionResult HPickup(PostViewModel model)
        {
            GetNameFromBu(model);

            // Liste des points de dépots multicritères
            PickupPointBusiness pickBu = new PickupPointBusiness();
            model.PickupPoints = pickBu.GetPickupPointsByMultiCriteria(model.City.Id, model.Volume.Id, model.StorageType.Id);

            return View(model);

        }

        [HttpPost]
        public IActionResult IStorage(PostViewModel model)
        {
            GetNameFromBu(model);
            StorageBusiness storageBusiness = new StorageBusiness();
            model.Storages = storageBusiness.GetStoragesByTypeAndVolumeOfAPickupPoint(model.StorageType.Id,model.Volume.Id,model.PickupPoint.Id);

            return View(model);
        }

        [HttpPost]
        public IActionResult JConfirmation(PostViewModel model)
        {
            GetNameFromBu(model);
            return View(model);
        }

        [HttpPost]
        public IActionResult KPublish(PostViewModel model)
        {
            GetNameFromBu(model);

            PostBusiness postBu = new PostBusiness();
            Post post = new Post();
            post.IdDonatingSubscriber = model.Subscriber.Id;
            post.IdStorage = model.Storage.Id;
            post.Title = model.Post.Title;
            post.Description = model.Post.Description;
            post.IdCityOfDeposit = model.City.Id;
            post.Picture = model.Post.Picture;
            postBu.SavePost(post);

            ProductLineBusiness productLineBu = new ProductLineBusiness();
            ProductLine productLine = new ProductLine();
            productLine.IdPost = post.Id;
            productLine.IdProduct = model.Product.Id;
            productLine.IdUnit = model.Unit.Id;
            productLine.Quantity = model.ProductLine.Quantity;
            productLine.BestBeforeDate = model.ProductLine.BestBeforeDate;
            productLineBu.CreateProductLine(productLine);

            return View(model);
        }
        #region Methodes
        public void GetNameFromBu(PostViewModel model)
        {
            if (model.Subscriber != null)
            {
                SubscriberBusiness subBu = new SubscriberBusiness();
                model.Subscriber = subBu.GetSubscriberById(model.Subscriber.Id);
            }

            if (model.Category != null) 
            {
            CategoryBusiness categoryBu = new CategoryBusiness();
            model.Category = categoryBu.GetCategory(model.Category.Id); 
            }
            
            if (model.SubCategory != null) 
            {
            SubCategoryBusiness subCategoryBu = new SubCategoryBusiness();
            model.SubCategory = subCategoryBu.GetSubCategory(model.SubCategory.Id);
            }

            if (model.ProductType != null)
            {
            ProductTypeBusiness productTypeBu = new ProductTypeBusiness();
            model.ProductType = productTypeBu.GetProductType(model.ProductType.Id);
            }
            
            if (model.Product != null)
            {
            ProductBusiness productBu = new ProductBusiness();
            model.Product = productBu.GetProduct(model.Product.Id);
            }

            if (model.Unit != null)
            {
            UnitBusiness unitBu = new UnitBusiness();
            model.Unit = unitBu.GetUnit(model.Unit.Id);
            }
            
            if (model.City != null)
            {
            CityBusiness cityBu = new CityBusiness();
            model.City = cityBu.GetCityById(model.City.Id);
            }
            
            if (model.Volume != null)
            {
            VolumeBusiness volumeBu = new VolumeBusiness();
            model.Volume = volumeBu.GetVolume(model.Volume.Id);
            }
            
            if(model.StorageType != null)
            {
            StorageTypeBusiness storageTypeBu = new StorageTypeBusiness();
            model.StorageType = storageTypeBu.GetStorageType(model.StorageType.Id);
            }

           if (model.PickupPoint != null)
            {
             PickupPointBusiness pickBu = new PickupPointBusiness();
             model.PickupPoint = pickBu.GetPickupPoint(model.PickupPoint.Id);
            }
            
           
        }
        #endregion
    }
}
