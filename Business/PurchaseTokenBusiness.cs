﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PurchaseTokenBusiness
    {
        public void SaveToken(TokenPurchase token)
        {
            TokenDAO dao = new TokenDAO();
            dao.Create(token);
        }

        public List<TokenPurchase> GetTokensByIdSub(int idSub)
        {
            TokenDAO dao = new TokenDAO();
            return dao.GetTokenPurchasesOfIdSubscriber(idSub);
        }

        public float GetAmountsOfIdSub(int idSub)
        {
            TokenDAO dao = new TokenDAO();
            List<TokenPurchase> tokenPuchases = dao.GetTokenPurchasesOfIdSubscriber(idSub);
            float amounts = 0;
            foreach (TokenPurchase token in tokenPuchases)
            {
                amounts += token.TokensNumber;
            }
            return amounts;
        }
    }
}
