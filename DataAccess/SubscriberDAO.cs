﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.DLD_GD.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class SubscriberDAO : DAO
    {
        public void Create(Subscriber subscriber)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO adherent
                                            (adherent, motDePasse, nom, prenom, dateDeNaissance, numRue, nomRue, telephone, mail, dateInscription, dateActivation, dateDesactivation, codeVerification)
                                            VALUES
                                            (@login, @password, @lastName, @name, @birthDate @streetNumber, @streetName, @phone, @mail, @subscriptionDate, @activationDate, @desactivationDate, @pinCode)";
            cmd.Parameters.Add(new MySqlParameter("login",subscriber.Login));
            cmd.Parameters.Add(new MySqlParameter("password",subscriber.Password));
            cmd.Parameters.Add(new MySqlParameter("lastName",subscriber.LastName));
            cmd.Parameters.Add(new MySqlParameter("name",subscriber.Name));
            cmd.Parameters.Add(new MySqlParameter("birthDate",subscriber.BirthDate));
            cmd.Parameters.Add(new MySqlParameter("streetNumber",subscriber.StreetNumber));
            cmd.Parameters.Add(new MySqlParameter("streetName",subscriber.StreetName));
            cmd.Parameters.Add(new MySqlParameter("phone",subscriber.PhoneNumber));
            cmd.Parameters.Add(new MySqlParameter("mail",subscriber.Email));
            cmd.Parameters.Add(new MySqlParameter("subscriptionDate", subscriber.SubscriptionDate));
            cmd.Parameters.Add(new MySqlParameter("pinCode",subscriber.PinCode));

           try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }


        private Subscriber DataReaderToEntity(DbDataReader dr)
        {
            Subscriber subscriber = new Subscriber();

            subscriber.Id = dr.GetInt32(dr.GetOrdinal("idAdherent"));
            subscriber.IdCredentials = dr.GetInt32(dr.GetOrdinal("idfonction"));
            subscriber.IdCity = dr.GetInt32(dr.GetOrdinal("idVille"));
            subscriber.Login = dr.GetString(dr.GetOrdinal("adherent"));
            subscriber.Password = dr.GetString(dr.GetOrdinal("motDePasse"));
            subscriber.LastName = dr.GetString(dr.GetOrdinal("nom"));
            subscriber.Name = dr.GetString(dr.GetOrdinal("prenom"));
            subscriber.BirthDate = dr.GetDateTime(dr.GetOrdinal("dateDeNaissance"));
            subscriber.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
            subscriber.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));
            subscriber.PhoneNumber = dr.GetString(dr.GetOrdinal("telephone"));
            subscriber.Email = dr.GetString(dr.GetOrdinal("mail"));
            subscriber.SubscriptionDate = dr.GetDateTime(dr.GetOrdinal("dateInscription"));
            if (!dr.IsDBNull(dr.GetOrdinal("dateActivation")))
            {
                subscriber.ActivationDate = dr.GetDateTime(dr.GetOrdinal("dateActivation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateDesactivation")))
            {
                subscriber.UnSubscriptionDate = dr.GetDateTime(dr.GetOrdinal("dateDesactivation"));
            }
            subscriber.PinCode = dr.GetInt32(dr.GetOrdinal("codeVerification"));

            return subscriber;
        }


        public Subscriber GetById(int id)
        {
            Subscriber result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM adherent WHERE idAdherent = @idSubscriber";
            cmd.Parameters.Add(new MySqlParameter("idSubscriber", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un adhérent : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }


        public List<Subscriber> GetAll()
        {
            List<Subscriber> result = new List<Subscriber>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM adherent";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Subscriber subscriber = DataReaderToEntity(dr);
                    result.Add(subscriber);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des adhérents : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }





    }
}
