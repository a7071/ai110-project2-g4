﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Post
    {
        public int Id { get; set; }
        public int IdDonatingSubscriber { get; set; }
        public int IdReceivingSubscriber { get; set; }
        public int IdStorage { get; set; }
        public DateTime PostDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Picture { get; set; }
        public DateTime? ReservationDate { get; set; }
        public DateTime? ValidationDate { get; set; }
        public DateTime? CancellationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
        public DateTime? DepositDate { get; set; }
        public DateTime? CollectionDate { get; set; }
        public int PinCode { get; set; }
        public int IdCityOfDeposit { get; set; }

        public Post()
        {
        }

        public Post(int id, int idDonatingSubscriber, int idReceivingSubscriber, int idStorage, DateTime postDate, string title,
            string description,string picture, DateTime reservationDate,DateTime validationDate, DateTime cancellationDate,DateTime deletionDate,DateTime depositDate,DateTime collectionDate, int pinCode, int idCityOfDeposit)
        {
            Id = id;
            IdDonatingSubscriber = idDonatingSubscriber;
            IdReceivingSubscriber = idReceivingSubscriber;
            IdStorage = idStorage;
            PostDate = DateTime.Now;
            Title = title;
            Description = description;
            Picture = picture;
            ReservationDate = reservationDate;
            ValidationDate = validationDate;
            CancellationDate = cancellationDate;
            DeletionDate = deletionDate;
            DepositDate = depositDate;
            CollectionDate = collectionDate;
            PinCode = 1234;
            IdCityOfDeposit = idCityOfDeposit;
        }
        
        

    }
}
