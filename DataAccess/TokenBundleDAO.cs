﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class TokenBundleDAO : DAO
    {
        public List<TokenBundle> GetTokenBundles()
        {
            List<TokenBundle> result = new List<TokenBundle>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM forfait";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    TokenBundle token = DataReaderToEntity(dr);
                    result.Add(token);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération de la liste de forfaits" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public TokenBundle GetTokenBundle(int id)
        {
            TokenBundle result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM forfait
                                         WHERE (idForfait = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);

                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération d'un forfait" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


        public TokenBundle DataReaderToEntity(DbDataReader dr)
        {
            TokenBundle token = new TokenBundle();
            token.Id = dr.GetInt32(dr.GetOrdinal("idForfait"));
            token.BundleName = dr.GetString(dr.GetOrdinal("nomForfait"));
            token.BundleAmount = dr.GetInt32(dr.GetOrdinal("nombreCreditsForfait"));
            token.BundlePrice = dr.GetFloat(dr.GetOrdinal("prixForfait"));
            return token;
        }
    }
}

