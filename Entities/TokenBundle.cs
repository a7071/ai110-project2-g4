﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class TokenBundle
    {
        public int Id { get; set; }
        public string BundleName { get; set; }
        public int BundleAmount { get; set; }
        public float BundlePrice { get; set; }

        public TokenBundle()
        {
        }

        public TokenBundle(int id, string bundleName, int bundleAmount, float bundlePrice)
        {
            Id = id;
            BundleName = bundleName;
            BundleAmount = bundleAmount;
            BundlePrice = bundlePrice;
        }
    }
}
