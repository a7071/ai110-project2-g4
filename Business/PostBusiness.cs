using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PostBusiness
    {
        public void SavePost(Post post)
        {
            PostDAO dao = new PostDAO();
            dao.Create(post);
        }

        public void Update(Post post)
        {
            PostDAO dao = new PostDAO();
            dao.Update(post);
        }

        public List<PostDetails> GetPosts()
        {
            PostDAO postDAO = new PostDAO();
            List<PostDetails> postsDetails = postDAO.GetAll();
            SetPostsAvaibility(postsDetails);
            return postsDetails;
        }

        public void SetPostsAvaibility(List<PostDetails> postsDetails)
        {
            foreach (PostDetails post in postsDetails)
            {
                if (post.DeletionDate == null)
                {
                    if (post.ReservationDate == null)
                    {
                        post.Availability = "Disponible";
                    }
                    else if (post.ReservationDate != null && post.ValidationDate == null)
                    {
                        post.Availability = "En attente de validation";
                    }
                    else if (post.ValidationDate != null && post.DepositDate == null)
                    {
                        post.Availability = "En attente de dépot";
                    }
                    else if (post.DepositDate != null && post.CollectionDate == null)
                    {
                        post.Availability = "En attente de collecte";
                    }
                    else if (post.CollectionDate != null)
                    {
                        post.Availability = "Terminé";
                    }
                } else
                {
                    post.Availability = "Annulé";
                }

            }

        }

        public List <PostDetails> OrderByPostDate(List <PostDetails> posts, string dateOrder)
        {
            List <PostDetails> result = new List<PostDetails>();

            switch (dateOrder)
            {
                case "AscendingPostDate":
                    result = posts.OrderBy(post => post.PostDate).ToList();
                    break;
                case "DescendingPostDate":
                    result = posts.OrderByDescending(post => post.PostDate).ToList();
                    break;
                case "AscendingBestBeforeDate":
                    result = posts.OrderBy(post => post.ProductLineDetails.BestBeforeDate).ToList();
                    break;
                case "DescendingBestBeforeDate":
                    result = posts.OrderByDescending(post => post.ProductLineDetails.BestBeforeDate).ToList();
                    break;
                default:
                    result = posts.OrderByDescending(post => post.PostDate).ToList();
                    break;
            }

            return result;
        }

        public List<PostDetails> SearchPosts(string postTitle, int idCity, int distance, int idSubscriber, string dateOrder, int idSearchedCategory)
        {
            PostDAO postDAO = new PostDAO();
            List<PostDetails> postsDetails = postDAO.GetByMultiCriteria(postTitle, idCity, distance, idSubscriber);
            SetPostsAvaibility(postsDetails);

            CityBusiness cityBU = new CityBusiness();
            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            ProductLineBusiness productLineBU = new ProductLineBusiness();
            foreach (PostDetails post in postsDetails)
            {
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                }
                post.ProductLineDetails = productLineBU.GetProductLineDetails(post.Id);

            }
            postsDetails = OrderByPostDate(postsDetails, dateOrder);

            if(idSearchedCategory != 0)
            {
                List<PostDetails> postsFilteredOnCategory = new List<PostDetails>();
                foreach(PostDetails postD in postsDetails){
                    if(postD.ProductLineDetails.ProductCategoryId == idSearchedCategory)
                    {
                        postsFilteredOnCategory.Add(postD);
                    }
                }
                return postsFilteredOnCategory;
            }
    
            return postsDetails;
        }

        public List<PostDetails> SearchPostsOfReceiver(string postTitle, int idCity, int distance, int idSubscriber)
        {
            PostDAO postDAO = new PostDAO();
            List<PostDetails> postsDetails = postDAO.GetPostOfReceiverByMultiCriteria(postTitle, idCity, distance, idSubscriber);
            SetPostsAvaibility(postsDetails);
            return postsDetails;
        }
        public List<PostDetails> GetPostsOfReceiver()
        {
            PostDAO postDAO = new PostDAO();
            List<PostDetails> postsDetails = postDAO.GetPostOfReceivers();
            SetPostsAvaibility(postsDetails);
            return postsDetails;
        }
        public List<PostDetails> GetPostsByIdSubscriber(int id)
        {
            PostDAO postDAO = new PostDAO();
            List <PostDetails> postsDetails = postDAO.GetByIdSubscriber(id);
            SetPostsAvaibility(postsDetails);
            return postsDetails;
        }

        public Post GetPostById(int id)
        {
            PostDAO postDAO = new PostDAO();
            return postDAO.GetById(id);
        }

        public List<Post> GetCurrentPosts(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCurrentByID(idSubscriber);
        }

        public List<Post> GetUnconfirmedPosts(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllUnconfirmedReservationsByID(idSubscriber);
        }

        public List<Post> GetCurrentReservations(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCurrentReservationsByID(idSubscriber);
        }

        public List<Post> GetConfirmedDeposits(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllConfirmedDepositsByID(idSubscriber);
        }

        public List<Post> GetPastReservations(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllPastReservationsByID(idSubscriber);
        }

        public List<Post> GetAwaitingDeposit(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllAwaitingDepositByID(idSubscriber);
        }

        public List<PostStorageDetails> GetPostStorageDetailsAwaitingDepositByPId(int idPartner)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllPostStorageDetailsAwaitingDepositByPID(idPartner);
        }

        public List<PostStorageDetails> GetPostStorageDetailsAwaitingPickupByPId(int idPartner)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllPostStorageDetailsAwaitingPickupByPID(idPartner);
        }

        public List<Post> GetPastDonations(int idSubscriber)
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllPastDonationsByID(idSubscriber);
        }

        public List<Post> GetAllCompleted()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCompleted();
        }

        public List<Post> GetAllCurrentExchanges()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCurrentExchanges();
        }

        public List<Post> GetAllCancelled()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCancelled();
        }

        public List<Post> GetAllCompletedThisMonth()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCompletedThisMonth();
        }

        public List<Post> GetAllPublishedThisMonth()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllPublishedThisMonth();
        }

        public List<Post> GetAllCancelledThisMonth()
        {
            PostDAO dao = new PostDAO();

            return dao.GetAllCancelledThisMonth();
        }

        public PostStorageDetails GetPostStorageDetailsByID(int idPost)
        {
            PostDAO dao = new PostDAO();

            return dao.GetPostStorageDetailsByID(idPost);
        }
    }
}
