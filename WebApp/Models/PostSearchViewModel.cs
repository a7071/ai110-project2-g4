using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class PostSearchViewModel
    {

        public const int DEFAULT_DISTANCE = 50;
        #region Critères de recherche
        public string Title { get; set; } = "";
        public int IdSearchedCity { get; set; } = 0;
        public int Distance { get; set; } = DEFAULT_DISTANCE;
        public int IdDonatingSubscriber { get; set; } = 0;
        public int IdReceivingSubscriber { get; set; } = 0;
        public List<string> DateOrderChoice { get; set; } = new List<string> { "AscendingPostDate", "DescendinPostDate", "AscendingBestBeforeDate", "DescendingBestBeforeDate" };
        public string DateOrder { get; set; } = "DescendinPostDate";
        public int IdSearchedCategory { get; set; }
        public bool IsRated { get; set; }


        #endregion


        #region Listes de données pour affichage
        public List<PostDetails> Posts { get; set; }
        public List<City> Cities { get; set; }
        public List<Subscriber> Subscribers { get; set; }
        public List<Category> Categories { get; set; }
        #endregion

    }
}
