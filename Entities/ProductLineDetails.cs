﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class ProductLineDetails : ProductLine
    {
        public string ProductName { get; set; } = "";
        public int ProductCategoryId { get; set; } = 0;
        public string ProductCategoryName { get; set; } = "";

        public ProductLineDetails() { 
        }

        public ProductLineDetails(ProductLine productLine)
        {
            IdPost = productLine.IdPost;
            IdProduct = productLine.IdProduct;
            IdUnit = productLine.IdUnit;
            Quantity = productLine.Quantity;
            BestBeforeDate = productLine.BestBeforeDate;
        }

    }
}
