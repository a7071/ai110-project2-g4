﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class VolumeDAO : DAO
    {
        public Volume GetVolume(int id)
        {
            Volume result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM volume 
                                         WHERE (idVolume = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<Volume> GetVolumes()
        {
            List<Volume> result = new List<Volume>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM volume";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Volume volume = DataReaderToEntity(dr);
                    result.Add(volume);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Volume DataReaderToEntity(DbDataReader dr)
        {
            Volume result = new Volume();
            result.Id = dr.GetInt32(dr.GetOrdinal("idVolume"));
            result.Format = dr.GetString(dr.GetOrdinal("format"));
            result.Height = dr.GetInt32(dr.GetOrdinal("hauteur"));
            result.Width = dr.GetInt32(dr.GetOrdinal("largeur"));
            result.Length = dr.GetInt32(dr.GetOrdinal("longueur"));
            return result;
        }
    }
}
