﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Schedule
    {
        public int Id { get; set; }
        public int IdDay { get; set; }

        public int IdPickupPoint { get; set; }
        public string OpeningHours { get; set; }
        public string ClosingHours { get; set; }

        public Schedule()
        {
        }

        public Schedule(int id, int idDay, int idPickupPoint, string openingHours, string closingHours)
        {
            Id = id;
            IdDay = idDay;
            IdPickupPoint = idPickupPoint;
            OpeningHours = openingHours;
            ClosingHours = closingHours;
        }
    }
}
