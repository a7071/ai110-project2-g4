﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class ProductTypeBusiness
    {
        public List<ProductType> GetProductTypesBySubCategories(int idSubCategory)
        {
            ProductTypeDAO dao = new ProductTypeDAO();
            return dao.GetProductTypeBySubCategory(idSubCategory);
        }

        public ProductType GetProductType(int id)
        {
            ProductTypeDAO dao = new ProductTypeDAO();
            return dao.GetProductType(id);
        }
    }
}
