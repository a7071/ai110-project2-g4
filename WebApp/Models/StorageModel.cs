﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class StorageModel
    {
        public Storage Storage { get; set; }
        public Volume Volume { get; set; }
        public PickupPoint PickupPoint { get; set; }
        public PickupPointDetails PickupDetails { get; set; }
        public List<Volume> Volumes { get; set; }

        public List<StorageType> Types { get; set; }

        public List<StorageDetail> Storages { get; set; }
    }
}
