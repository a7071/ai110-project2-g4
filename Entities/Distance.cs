﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Distance
    {
        public int IdFirstCity { get; set; }
        public int IdSecondCity { get; set;}

        public int AverageDistance { get; set; }

    }
}
