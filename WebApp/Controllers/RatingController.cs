﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class RatingController : Controller
    {
        [HttpGet]
        public IActionResult IndexReceiver(int id)
        {
            RatingViewModel model = new RatingViewModel();
            model.IdPost = id;

            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);

            SubscriberBusiness subBu = new SubscriberBusiness();
            model.RatingSub = subBu.GetSubscriberById(post.IdReceivingSubscriber);
            model.RatedSub = subBu.GetSubscriberById(post.IdDonatingSubscriber);

            model.IsDonator = false;

            return View("Index",model);
        }
        [HttpPost]
        public IActionResult SaveRating(RatingViewModel model)
        {
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(model.IdPost);

            SubscriberBusiness subBu = new SubscriberBusiness();
            RatingBusiness ratingBu = new RatingBusiness();
            model.Rating.IdPost = model.IdPost;

            if (model.IsDonator)
            {
                model.RatingSub = subBu.GetSubscriberById(post.IdDonatingSubscriber);
                model.RatedSub = subBu.GetSubscriberById(post.IdReceivingSubscriber);

                model.Rating.IdSubscriber = post.IdDonatingSubscriber;
                model.Rating.IdReceivingSub = post.IdReceivingSubscriber;
                
            }
            else
            {
                model.RatingSub = subBu.GetSubscriberById(post.IdReceivingSubscriber);
                model.RatedSub = subBu.GetSubscriberById(post.IdDonatingSubscriber);

                model.Rating.IdSubscriber = post.IdReceivingSubscriber;
                model.Rating.IdReceivingSub = post.IdDonatingSubscriber;
            }

            ratingBu.Save(model.Rating);
            model.Message = "Votre note a bien été prise en compte";

            return View("Index",model);
        }

        public IActionResult IndexDonator(int id)
        {
            RatingViewModel model = new RatingViewModel();
            model.IdPost = id;

            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);

            SubscriberBusiness subBu = new SubscriberBusiness();
            model.RatingSub = subBu.GetSubscriberById(post.IdDonatingSubscriber); 
            model.RatedSub = subBu.GetSubscriberById(post.IdReceivingSubscriber);

            RatingBusiness ratBu = new RatingBusiness();
            model.Rating = ratBu.GetRatingByIdPost(model.RatingSub.Id, model.IdPost);

            model.IsDonator = true;

            return View("Index", model);
        }

        public IActionResult Display()
        {
            RatingViewModel model = new RatingViewModel();

            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscribers = subBu.GetSubscribers();

            RatingBusiness ratBu = new RatingBusiness();
            model.Ratings = ratBu.GetRatings(0);

            return View(model);
        }
        [HttpPost]
        public IActionResult Display(RatingViewModel model)
        {
            SubscriberBusiness subBu = new SubscriberBusiness();
            model.Subscribers = subBu.GetSubscribers();

            RatingBusiness ratBu = new RatingBusiness();
            model.Ratings = ratBu.GetRatings(model.RatedSub.Id);

            return View(model);
        }

    }
}
