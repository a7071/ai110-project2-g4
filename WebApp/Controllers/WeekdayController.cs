﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;

namespace WebApp.Controllers
{
    public class WeekdayController : Controller
    {
        public IActionResult Index()
        {
            WeekdayBusiness weekdaybu = new WeekdayBusiness();

            List<Weekday> weekdays = weekdaybu.GetWeekdays();
            ViewBag.Weekdays = weekdays;

            return View();
        }

    }
}
