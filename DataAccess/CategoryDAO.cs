﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class CategoryDAO : DAO
    {
        public Category getCategory(int id)
        {
            Category result = new Category();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM categorie
                                        WHERE idCategorie = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            return result;
        }

        public List<Category> getCategories()
        {
            List<Category> result = new List<Category>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM categorie";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Category category = new Category();
                    category = DataReaderToEntity(dr);
                    result.Add(category);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Category DataReaderToEntity(DbDataReader dr)
        {
            Category category = new Category();

            category.Id = dr.GetInt32(dr.GetOrdinal("idCategorie"));
            category.CategoryName = dr.GetString(dr.GetOrdinal("categorie"));

            return category;
        }
    }
}
