﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class ProductBusiness
    {
        public List<Product> GetProductsByProductType(int idProductType)
        {
            ProductDAO dao = new ProductDAO();
            return dao.GetProductByProductType(idProductType);
        }

        public Product GetProduct(int id)
        {
            ProductDAO dao = new ProductDAO();
            return dao.GetProduct(id);
        }
    }
}
