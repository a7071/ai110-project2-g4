using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class StorageBusiness
    {
        public Storage SaveStorage(Storage storage)
        {
            StorageDAO storageDAO = new StorageDAO();

            storage = storageDAO.InsertNewStorage(storage);

            return storage;
        }

        public void DesactivateStorage(int id)
        {
            StorageDAO storageDAO = new StorageDAO();
            Storage storage = storageDAO.GetStorage(id);
            storage.DeletionDate = DateTime.Now;
            storageDAO.UpdateStorage(storage);
        }

        public DateTime FindCurrentTime()
        {
            DateTime now = DateTime.Now.Date;
            return now;
        }

        public List<StorageDetail> ShowStorageDetails(int idPickupPt)
        {
            StorageDAO storageDAO = new StorageDAO();

            return storageDAO.GetStorageVolumeAndTypeByID(idPickupPt);
        }

        public List<Storage> GetStoragesByTypeAndVolume(int idType, int idVolume)
        {
            StorageDAO dao = new StorageDAO();
            return dao.GetStoragesByTypeAndVolume(idType, idVolume);
        }

        public Storage GetStorage(int id)
        {
            StorageDAO dao = new StorageDAO();
            return dao.GetStorage(id);
        }

        public List<Storage> GetStoragesByTypeAndVolumeOfAPickupPoint(int idType, int idVolume, int idPickuPoint)
        {
            StorageDAO dao = new StorageDAO();
            return dao.GetStoragesByTypeAndVolumeOfAPickupPoint(idType, idVolume, idPickuPoint);
        }
    }
}
