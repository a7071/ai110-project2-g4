﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class PartnerDAO : DAO
    {
        
        public void Create(Partner partner)
        {
            DbConnection cnx = new MySqlConnection();
            cnx.ConnectionString = (CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO partenaire
                                            (idVille, partenaire, motDePasse, nomRepresentant, prenomRepresentant, telephoneRepresentant, mailRepresentant, nomInfrastructure, numSiret, numRue, nomRue, telephone, email, dateInscription, dateActivation, dateDesactivation, codeVerification)
                                            VALUES
                                            (@idCity, @login, @password, @lastName, @name, @phone, @mail, @infrastructurename, @siretNumber, @streetNumber, @streetName, @phone, @mail, @subscriptionDate, @activationDate, @desactivationDate, @pinCode)";
            cmd.Parameters.Add(new MySqlParameter("idCity", partner.IdCity));
            cmd.Parameters.Add(new MySqlParameter("login", partner.PartnerLogin));
            cmd.Parameters.Add(new MySqlParameter("password", partner.PartnerPassword));
            cmd.Parameters.Add(new MySqlParameter("lastName", partner.RepresentativeLastName));
            cmd.Parameters.Add(new MySqlParameter("name", partner.RepresentativeName));    
            cmd.Parameters.Add(new MySqlParameter("phone", partner.RepresentativePhone));
            cmd.Parameters.Add(new MySqlParameter("mail", partner.RepresentativeMail));
            cmd.Parameters.Add(new MySqlParameter("infrasctructure", partner.InfrastructureName));
            cmd.Parameters.Add(new MySqlParameter("siret", partner.SiretNumber));
            cmd.Parameters.Add(new MySqlParameter("streetnumber", partner.StreetNumber));
            cmd.Parameters.Add(new MySqlParameter("streetname", partner.StreetName));
            cmd.Parameters.Add(new MySqlParameter("phone", partner.PhoneNumber));
            cmd.Parameters.Add(new MySqlParameter("mail", partner.Email));
            cmd.Parameters.Add(new MySqlParameter("subscriptionDate", partner.SubscriptionDate));
            cmd.Parameters.Add(new MySqlParameter("activation", partner.ActivationDate));
            cmd.Parameters.Add(new MySqlParameter("desactivation", partner.UnSubscriptionDate));
            cmd.Parameters.Add(new MySqlParameter("pinCode", partner.PinCode));

            try
            {
                cnx.Open();

                cmd.ExecuteNonQuery();
            }
                catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

            finally { 
                
                cnx.Close(); 
            
            }
               
        }
        

        private Partner DataReaderToEntity(DbDataReader dr)
        {
            Partner partner = new Partner();

            partner.Id = dr.GetInt32(dr.GetOrdinal("idPartenaire"));
            partner.IdCity = dr.GetInt32(dr.GetOrdinal("idVille"));
            partner.PartnerLogin = dr.GetString(dr.GetOrdinal("partenaire"));
            partner.PartnerPassword = dr.GetString(dr.GetOrdinal("motDePasse"));
            partner.RepresentativeLastName = dr.GetString(dr.GetOrdinal("nomRepresentant"));
            partner.RepresentativeName = dr.GetString(dr.GetOrdinal("prenomRepresentant"));
            partner.RepresentativePhone = dr.GetString(dr.GetOrdinal("telephoneRepresentant"));
            partner.RepresentativeMail = dr.GetString(dr.GetOrdinal("mailRepresentant"));
            partner.InfrastructureName = dr.GetString(dr.GetOrdinal("nomInfrastructure"));
            partner.SiretNumber = dr.GetString(dr.GetOrdinal("numSiret"));
            partner.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
            partner.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));
            partner.PhoneNumber = dr.GetString(dr.GetOrdinal("telephone"));
            partner.Email = dr.GetString(dr.GetOrdinal("email"));
            partner.SubscriptionDate = dr.GetDateTime(dr.GetOrdinal("dateInscription"));
            if (!dr.IsDBNull(dr.GetOrdinal("dateActivation")))
            {
                partner.ActivationDate = dr.GetDateTime(dr.GetOrdinal("dateActivation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateDesactivation")))
            {
                partner.UnSubscriptionDate = dr.GetDateTime(dr.GetOrdinal("dateDesactivation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("codeVerification")))
            {
                partner.PinCode = dr.GetInt32(dr.GetOrdinal("codeVerification"));
            }
            return partner;
        }


        public Partner GetById(int id)
        {
            Partner result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM partenaire WHERE idPartenaire = @idPartner";

            cmd.Parameters.Add(new MySqlParameter("idPartner", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'un partenaire : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }


        public List<Partner> GetAll()
        {
            List<Partner> result = new List<Partner>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM partenaire";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Partner partner = DataReaderToEntity(dr);
                    result.Add(partner);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des partenaires : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }





    }
}
