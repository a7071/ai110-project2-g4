﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class UnitDAO : DAO
    {
        public List<Unit> GetUnits()
        {
            List<Unit> result = new List<Unit>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM unite";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Unit unit = new Unit();
                    unit = DataReaderToEntity(dr);
                    result.Add(unit);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;

        }

        public List<int> GetUnitsByProduct(int idProduct)
        {
            List<int> result = new List<int>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT p.*,m.* FROM produit p JOIN mesurer m ON p.idProduit = m.idProduit 
                                                   WHERE (m.idProduit = @idProduit)";
            cmd.Parameters.Add(new MySqlParameter("idProduit", idProduct));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    int id = dr.GetInt32(dr.GetOrdinal("idUnite"));
                    result.Add(id);

                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Unit GetUnit(int id)
        {
            Unit result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM unite
                                         WHERE (idUnite = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);

                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public Unit DataReaderToEntity(DbDataReader dr)
        {
            Unit unit = new Unit();
            unit.Id = dr.GetInt32(dr.GetOrdinal("idUnite"));
            unit.UnitName = dr.GetString(dr.GetOrdinal("unite"));
            return unit;
        }
    }
}
