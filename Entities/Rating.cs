﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Rating
    {
        public int Id { get; set; }
        public int IdSubscriber { get; set; }
        public int IdReceivingSub { get; set; }
        public int IdPost { get; set; }
        public int Score{ get; set; }
        public string Comment { get; set; }
        public DateTime ScoreDate { get; set; }

        public Rating()
        {
        }

        public Rating(int id, int idSubscriber, int idReceivingSub,int idPost, int score, string comment, DateTime scoreDate)
        {
            Id = id;
            IdSubscriber = idSubscriber;
            IdReceivingSub = idReceivingSub;
            IdPost = idPost;
            Score = score;
            Comment = comment;
            ScoreDate = scoreDate;
        }
    }
}
