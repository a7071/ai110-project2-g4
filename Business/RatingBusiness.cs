﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class RatingBusiness
    {
        public void Save(Rating rating)
        {
            RatingDAO dao = new RatingDAO();
            dao.Create(rating);
        }
        public List<Rating> GetRatings(int idPost)
        {
            RatingDAO dao = new RatingDAO();
            return dao.GetRatings(idPost);
        }

        public Rating GetRatingByIdPost(int idRatingSub, int idPost)
        {
            RatingDAO dao = new RatingDAO();
            return dao.GetRatingByIdSubOfIdPost(idRatingSub, idPost);
        }
    }
}
