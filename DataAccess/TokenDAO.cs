﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class TokenDAO : DAO
    {
        public void Create(TokenPurchase token)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO achatdecredit
                                (idAdherent,dateAchat,nombreCredits,montantAchat)
                                VALUES
                                (@idSubscriber,Date(SYSDATE()),@tokenAmount,@amount);
                                SELECT LAST_INSERT_ID();";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber",token.IdSubscriber));
            cmd.Parameters.Add(new MySqlParameter("tokenAmount", token.TokensNumber));
            cmd.Parameters.Add(new MySqlParameter("amount", token.PurchaseAmount));

            try
            {
                cnx.Open();

                token.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion d'un achat de crédit." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public List<TokenPurchase> GetTokenPurchasesOfIdSubscriber(int idSubscriber)
        {
            List<TokenPurchase> result = new List<TokenPurchase>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM achatdecredit
                                         WHERE (idAdherent = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    TokenPurchase token = DataReaderToEntity(dr);
                    result.Add(token);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération d'un achat de crédit" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public TokenPurchase DataReaderToEntity(DbDataReader dr)
        {
            TokenPurchase result = new TokenPurchase();
            result.Id = dr.GetInt32(dr.GetOrdinal("idAchatCredit"));
            result.IdSubscriber = dr.GetInt32(dr.GetOrdinal("idAdherent"));
            result.PurchaseDate = dr.GetDateTime(dr.GetOrdinal("dateAchat"));
            result.TokensNumber = dr.GetInt32(dr.GetOrdinal("nombreCredits"));
            result.PurchaseAmount = dr.GetFloat(dr.GetOrdinal("montantAchat"));

            return result;
        }
    }
}
