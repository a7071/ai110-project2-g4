﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PurchaseLineBusiness
    {
        public void SavePurchaseLine(PurchaseLine line)
        {
            PurchaseLineDAO dao = new PurchaseLineDAO();
            dao.Create(line);
        }
    }
}
