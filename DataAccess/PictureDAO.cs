﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class PictureDAO : DAO
    {

        public void Insert(Picture picture)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"INSERT INTO photo
                                (idAnnonce, chemin) 
                                VALUES 
                                (@idPost, @picturePath)";

            cmd.Parameters.Add(new MySqlParameter("idPost", picture.IdPost));
            cmd.Parameters.Add(new MySqlParameter("picturePath", picture.PicturePath));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur lors de l'insertion d'une photo : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }


        public void Delete(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"DELETE FROM photo 
                                WHERE idPhoto = @idPicture";

            cmd.Parameters.Add(new MySqlParameter("idPicture", id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de suppression d'une photo : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
        }


        private Picture DataReaderToEntity(DbDataReader dr)
        {
            Picture picture = new Picture();
            picture.Id = dr.GetInt32(dr.GetOrdinal("idPhoto"));
            if (!dr.IsDBNull(dr.GetOrdinal("idAnnonce")))
            {
                picture.IdPost = dr.GetInt32(dr.GetOrdinal("idAnnonce"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("chemin")))
            {
                picture.PicturePath = dr.GetString(dr.GetOrdinal("chemin"));
            }

            return picture;
        }



        public List<Picture> GetAll()
        {
            List<Picture> result = new List<Picture>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM photo";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Picture picture = DataReaderToEntity(dr);
                    result.Add(picture);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des photos : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


        public List<Picture> GetById(int id)
        {
            List<Picture> result = new List<Picture>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM photo WHERE idAnnonce = @idPost";

            cmd.Parameters.Add(new MySqlParameter("idPost", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Picture picture = DataReaderToEntity(dr);
                    result.Add(picture);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des photos de l'annonce " + id + ": " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }


    }
}