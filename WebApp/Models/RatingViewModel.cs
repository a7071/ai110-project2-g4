﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class RatingViewModel
    {
        public Rating Rating { get; set; }
        public int IdPost { get; set; }
        public Post Post { get; set; }
        public Subscriber RatingSub { get; set; }
        public Subscriber RatedSub { get; set; }
        public string Message { get; set; } = "";
        public bool IsDonator { get; set; }

        public List<Subscriber> Subscribers { get; set;}
        public List<Rating> Ratings { get; set; }
    }
}
