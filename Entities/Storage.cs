﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Storage
    {
        public int Id { get; set; }
        public int IdVolume { get; set; }
        public int IdStorageType { get; set; }
        public int IdPickupPoint { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime? DeletionDate { get; set; }
        public DateTime? CancellationDate { get; set; }

        public Storage()
        {
        }
        public Storage(int id, int idVolume, int idStorageType, int idPickupPoint, DateTime creationDate, DateTime deletionDate, DateTime cancellationDate)
        {
            Id = id;
            IdVolume = idVolume;
            IdStorageType = idStorageType;
            IdPickupPoint = idPickupPoint;
            CreationDate = creationDate;
            DeletionDate = deletionDate;
            CancellationDate = cancellationDate;
        }
    }
}
