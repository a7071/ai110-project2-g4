using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class StorageTypeBusiness
    {
        public List<StorageType> GetStorageTypes()
        {
            StorageTypeDAO dao = new StorageTypeDAO();

            List<StorageType> types = dao.GetStorageTypes();

            return types;
        }

        public List<StorageType> GetStorageTypesByIdCategories(int idCategory)
        {
            List<StorageType> result = new List<StorageType>();

            StorageTypeDAO dao = new StorageTypeDAO();
            dao.GetStorageTypeIdsByCategory(idCategory);

            foreach (int id in dao.GetStorageTypeIdsByCategory(idCategory))
            {
                result.Add(dao.GetStorage(id));
            }
            return result;

        }

        public StorageType GetStorageType(int id)
        {
            StorageTypeDAO dao = new StorageTypeDAO();
            return dao.GetStorage(id);
        }
    }
}
