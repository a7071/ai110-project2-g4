﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class PostViewModel
    {
        #region Critères
        public Post Post { get; set; }
        public Subscriber Subscriber { get; set; }
        public Category Category { get; set; }
        public SubCategory SubCategory { get; set; }
        public ProductType ProductType { get; set; }
        public Product Product { get; set; }
        public ProductLine ProductLine { get; set; }
        public Unit Unit { get; set; }
        public City City { get; set; }
        public PickupPoint PickupPoint { get; set; }
        public StorageType StorageType { get; set; }
        public Volume Volume { get; set; }
        public Storage Storage { get; set; }
        public IFormFile MyFile { get; set; }
        #endregion

        #region Affichage
        public List<PickupPoint> PickupPoints { get; set;}
        public List<City> Cities { get; set; }
        public List<Product> Products { get; set; }
        public List<ProductType> ProductTypes { get; set; }
        public List<SubCategory> SubCategories { get; set; }
        public List<Category> Categories { get; set; }
        public List<Unit> Units { get; set; }
        public List<StorageType> StorageTypes { get; set; }
        public List<Volume> Volumes { get; set; }
        public List<Storage> Storages { get; set; }
        public List<Subscriber> Subscribers { get; set; }
        #endregion
    }
}
