﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
   public class CategoryBusiness
    {
        public Category GetCategory(int id)
        {
            CategoryDAO dao = new CategoryDAO();
            return dao.getCategory(id);
        }
        public List<Category> GetCategories()
        {
            List<Category> categories = new List<Category>();
            CategoryDAO dao = new CategoryDAO();
            return dao.getCategories();
        }
    }
}
