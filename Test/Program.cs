﻿// See https://aka.ms/new-console-template for more information

using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;

StorageDAO StorageDAO = new StorageDAO();
StorageTypeDAO stdao = new StorageTypeDAO();
PostDAO postdao = new PostDAO();

List<Post> posts = postdao.GetAllPastReservationsByID(25);

foreach (Post post in posts)
{
    Console.WriteLine(post.Title);
}