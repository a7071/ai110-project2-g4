﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class ProductType
    {
        public int Id { get; set; }
        public int IdSubCategory { get; set; }
        public string ProductTypeName { get; set; }

    }
}
