﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class ScheduleBusiness
    {
        public void InsertSchedule(Schedule schedule)
        {
            ScheduleDAO dao = new ScheduleDAO();

            dao.InsertPPSchedule(schedule);
        }

        public void UpdateSchedule(Schedule schedule)
        {
            ScheduleDAO dao = new ScheduleDAO();
            dao.Update(schedule);
        }


        public List<Schedule> GetPickupSchedules()
        {
            ScheduleDAO dao = new ScheduleDAO();
            return dao.GetAll();
        }

        public List<ScheduleDetails> GetSchedulesByIdPickup(int pickupId)
        {
            ScheduleDAO dao = new ScheduleDAO();
            return dao.GetByIdPickup(pickupId);
        }

    }
}
