﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Subscriber
    {
        public int Id { get; set; }
        public int IdCredentials { get; set; }
        public int IdCity { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public string LastName { get; set; }
        public string Name { get; set; }
        public DateTime BirthDate { get; set; }
        public string StreetName { get; set; }
        public string StreetNumber { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public int PinCode { get; set; }
        public DateTime SubscriptionDate { get; set; }
        public DateTime ActivationDate { get; set; }
        public DateTime UnSubscriptionDate { get; set; }

        public Subscriber()
        {
        }

        public Subscriber(int id, int idCredentials, int idCity, 
            string login, string password, string lastName, string name, DateTime birthDate, string streetName, 
            string streetNumber, string phoneNumber, string email, int pinCode, DateTime subscriptionDate, DateTime unSubscriptionDate)
        {
            Id = id;
            IdCredentials = idCredentials;
            IdCity = idCity;
            Login = login;
            Password = password;
            LastName = lastName;
            Name = name;
            BirthDate = birthDate;
            StreetName = streetName;
            StreetNumber = streetNumber;
            PhoneNumber = phoneNumber;
            Email = email;
            PinCode = pinCode;
            SubscriptionDate = subscriptionDate;
            UnSubscriptionDate = unSubscriptionDate;
        }
    }
}
