﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class TokenViewModel
    {
        public Subscriber Subscriber { get; set; }
        public TokenPurchase TokenPurchase { get; set; }
        public PurchaseLine PurchaseLine { get; set; }
        public TokenBundle TokenBundle { get; set; }
        public float Donation { get; set; }
        public float Received { get; set; }
        public float Amount { get; set; }
        public float Calculate { get; set; }
        public string Message { get; set; } = "";
        public List<TokenBundle> Bundles { get; set; }
        public List<Subscriber> Subscribers { get; set; }
    }
}
