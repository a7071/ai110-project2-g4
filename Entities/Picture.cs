﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Picture
    {
        public int Id { get; set; }
        public int IdPost { get; set; }
        public string PicturePath { get; set; }


        public override string ToString()
        {
            return PicturePath;
        }

    }
}
