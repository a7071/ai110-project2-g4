﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Fr.EQL.AI110.DLD_GD.Entities;
using System.Data.Common;
using MySql.Data.MySqlClient;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class PostDAO : DAO
    {
        public void Create(Post post)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO annonce
                                            (idAdherent,idEmplacement,titre,description,photo,datePublication,dateReservation,dateValidation,dateAnnulation,dateRetrait,dateDepot,dateCollecte,codeEchange,idVilleDepot)
                                       VALUES
                                            (@idDonatingSubscriber,@idStorage,@title,@description,@picture,Date(SYSDATE()),@reservationDate,@validationDate,@cancellationDate,@deletionDate,@depositDate,@collectionDate,@exchangeCode,@idCityDeposit);
                               SELECT LAST_INSERT_ID() FROM annonce;
                                ";
            cmd.Parameters.Add(new MySqlParameter("idDonatingSubscriber",post.IdDonatingSubscriber));

            if (post.IdStorage == 0)
            {
            cmd.Parameters.Add(new MySqlParameter("idStorage", null));
            }
            else
            {
            cmd.Parameters.Add(new MySqlParameter("idStorage", post.IdStorage));
            }

            cmd.Parameters.Add(new MySqlParameter("title",post.Title));
            cmd.Parameters.Add(new MySqlParameter("description",post.Description));
            cmd.Parameters.Add(new MySqlParameter("picture",post.Picture));
            cmd.Parameters.Add(new MySqlParameter("reservationDate",post.ReservationDate));
            cmd.Parameters.Add(new MySqlParameter("validationDate",post.ValidationDate));
            cmd.Parameters.Add(new MySqlParameter("cancellationDate",post.CancellationDate));
            cmd.Parameters.Add(new MySqlParameter("deletionDate",post.DeletionDate));
            cmd.Parameters.Add(new MySqlParameter("depositDate", post.DepositDate));
            cmd.Parameters.Add(new MySqlParameter("collectionDate", post.CollectionDate));
            cmd.Parameters.Add(new MySqlParameter("exchangeCode",1234));
            cmd.Parameters.Add(new MySqlParameter("idCityDeposit",post.IdCityOfDeposit));

            try
            {
                cnx.Open();
                post.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur de la publication d'une annonce : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

        }
        public void Update(Post post)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE annonce
                                       SET dateReservation = @reservationDate,
                                           dateValidation = @validationDate,
                                           dateAnnulation = @cancellationDate,
                                           dateRetrait = @deletionDate,
                                           dateDepot = @depositDate,
                                           dateCollecte = @collectionDate,
                                           Beneficiaire_idAdherent = @idReceiver
                                       WHERE idAnnonce = @id";
            cmd.Parameters.Add(new MySqlParameter("id", post.Id));
            cmd.Parameters.Add(new MySqlParameter("reservationDate", post.ReservationDate));
            cmd.Parameters.Add(new MySqlParameter("validationDate", post.ValidationDate));
            cmd.Parameters.Add(new MySqlParameter("cancellationDate", post.CancellationDate));
            cmd.Parameters.Add(new MySqlParameter("deletionDate", post.DeletionDate));
            cmd.Parameters.Add(new MySqlParameter("depositDate", post.DepositDate));
            cmd.Parameters.Add(new MySqlParameter("collectionDate", post.CollectionDate));
            if (post.IdReceivingSubscriber == 0)
            {
             cmd.Parameters.Add(new MySqlParameter("idReceiver", null));
            }
            else
            {
             cmd.Parameters.Add(new MySqlParameter("idReceiver", post.IdReceivingSubscriber));
            }

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch(MySqlException ex)
            {
                throw new Exception ("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close ();
            }
        }
        private Post DataReaderToEntity(DbDataReader dr)
        {
            Post post = new Post();

            post.Id = dr.GetInt32(dr.GetOrdinal("idAnnonce"));
            if (!dr.IsDBNull(dr.GetOrdinal("idAdherent")))
            {
                post.IdDonatingSubscriber = dr.GetInt32(dr.GetOrdinal("idAdherent"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("idEmplacement")))
            {
                post.IdStorage = dr.GetInt32(dr.GetOrdinal("idEmplacement"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("Beneficiaire_idAdherent")))
            {
                post.IdReceivingSubscriber = dr.GetInt32(dr.GetOrdinal("Beneficiaire_idAdherent"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("titre")))
            {
                post.Title = dr.GetString(dr.GetOrdinal("titre"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("description")))
            {
                post.Description = dr.GetString(dr.GetOrdinal("description"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("photo")))
            {
                post.Picture = dr.GetString(dr.GetOrdinal("photo"));
            }
                post.PostDate = dr.GetDateTime(dr.GetOrdinal("datePublication"));

            if (!dr.IsDBNull(dr.GetOrdinal("dateReservation")))
            {
                post.ReservationDate = dr.GetDateTime(dr.GetOrdinal("dateReservation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateValidation")))
            {
                post.ValidationDate = dr.GetDateTime(dr.GetOrdinal("dateValidation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateAnnulation")))
            {
                post.CancellationDate = dr.GetDateTime(dr.GetOrdinal("dateAnnulation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateRetrait")))
            {
                post.DeletionDate = dr.GetDateTime(dr.GetOrdinal("dateRetrait"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateDepot")))
            {
                post.DepositDate = dr.GetDateTime(dr.GetOrdinal("dateDepot"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateCollecte")))
            {
                post.CollectionDate = dr.GetDateTime(dr.GetOrdinal("dateCollecte"));
            }
            post.PinCode = dr.GetInt32(dr.GetOrdinal("codeEchange"));
            if (!dr.IsDBNull(dr.GetOrdinal("idVilleDepot")))
            {
                post.IdCityOfDeposit = dr.GetInt32(dr.GetOrdinal("idVilleDepot"));
            }

            return post;
        }
        public Post GetById(int id)
        {
            Post result = null;

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM annonce WHERE idAnnonce = @idPost";
            cmd.Parameters.Add(new MySqlParameter("idPost", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de récupération d'une annonce : " + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<PostDetails> GetAll()
        {
            List<PostDetails> result = new List<PostDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM annonce";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostDetails postDetails = new PostDetails(post);
                    result.Add(postDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<PostDetails> GetByMultiCriteria(string postTitle, int idCity, int distance, int idSubscriber)
        {
            List<PostDetails> result = new List<PostDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT a.*, d.distanceMoyenne
                                FROM annonce a
                                LEFT JOIN distance d ON a.idVilleDepot = d.idVille 
                                WHERE (a.titre LIKE @title)
                                AND (VILLE_idVille = @idCity OR @idCity=0)
                                AND (distanceMoyenne <= @distance)
                                AND (idAdherent = @idSubscriber OR @idSubscriber=0)
                                ";

            postTitle = "%" + postTitle + "%";
            cmd.Parameters.Add(new MySqlParameter("title", postTitle));
            cmd.Parameters.Add(new MySqlParameter("idCity", idCity));
            cmd.Parameters.Add(new MySqlParameter("distance", distance));
            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostDetails postDetails = new PostDetails(post);
                    if (!dr.IsDBNull(dr.GetOrdinal("distanceMoyenne")))
                    {
                        postDetails.Distance = dr.GetInt32(dr.GetOrdinal("distanceMoyenne"));
                    }
                    result.Add(postDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<PostDetails> GetPostOfReceiverByMultiCriteria(string postTitle, int idCity, int distance, int idSubscriber)
        {
            List<PostDetails> result = new List<PostDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT a.*, d.distanceMoyenne
                                FROM annonce a
                                LEFT JOIN distance d ON a.idVilleDepot = d.idVille 
                                WHERE (a.titre LIKE @title)
                                AND (VILLE_idVille = @idCity OR @idCity=0)
                                AND (distanceMoyenne <= @distance)
                                AND (Beneficiaire_idAdherent = @idSubscriber OR @idSubscriber=0)
                                ";

            postTitle = "%" + postTitle + "%";
            cmd.Parameters.Add(new MySqlParameter("title", postTitle));
            cmd.Parameters.Add(new MySqlParameter("idCity", idCity));
            cmd.Parameters.Add(new MySqlParameter("distance", distance));
            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostDetails postDetails = new PostDetails(post);
                    if (!dr.IsDBNull(dr.GetOrdinal("distanceMoyenne")))
                    {
                        postDetails.Distance = dr.GetInt32(dr.GetOrdinal("distanceMoyenne"));
                    }
                    result.Add(postDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }
        public List<PostDetails> GetPostOfReceivers()
        {
            List<PostDetails> result = new List<PostDetails>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM annonce
                                         WHERE Beneficiaire_idAdherent is not null";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostDetails postDetails = new PostDetails(post);
                    result.Add(postDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<PostDetails> GetByIdSubscriber(int idSubscriber)
        {
            List<PostDetails> result = new List<PostDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM annonce WHERE idAdherent = @idSubscriber";
            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostDetails postDetails = new PostDetails(post);
                    result.Add(postDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCurrentByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (idAdherent=@idSubscriber AND dateReservation IS NULL) 
                                OR (idAdherent=@idSubscriber AND dateReservation LIKE '0001-01-01%'); ";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllUnconfirmedReservationsByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE idAdherent=@idSubscriber
                                AND (dateReservation IS NOT NULL AND dateReservation NOT LIKE '0001-01-01%') 
                                AND (dateValidation IS NULL OR dateValidation LIKE '0001-01-01%'); ";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCurrentReservationsByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE Beneficiaire_idAdherent=@idSubscriber 
                                AND (dateReservation IS NOT NULL AND dateReservation NOT LIKE '0001-01-01%')
                                AND (dateDepot IS NULL OR dateDepot LIKE '0001-01-01%');";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllConfirmedDepositsByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE Beneficiaire_idAdherent=@idSubscriber
                                AND (dateDepot IS NOT NULL AND dateDepot NOT LIKE '0001-01-01%') 
                                AND (dateCollecte IS NULL OR dateCollecte LIKE '0001-01-01%');";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllPastReservationsByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DateOnly currentDate = DateOnly.FromDateTime(DateTime.Today);
            string sqlDate = currentDate.ToString("yyyy-MM-dd");

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE Beneficiaire_idAdherent=@idSubscriber 
                                AND (dateCollecte IS NOT NULL AND dateCollecte NOT LIKE '0001-01-01%')
                                AND (dateCollecte <= @currentDate);";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));
            cmd.Parameters.Add(new MySqlParameter("currentDate", sqlDate));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllAwaitingDepositByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE idAdherent=@idSubscriber
                                AND (dateValidation IS NOT NULL AND dateValidation NOT LIKE '0001-01-01%') 
                                AND (dateDepot IS NULL OR dateDepot LIKE '0001-01-01%');";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<PostStorageDetails> GetAllPostStorageDetailsAwaitingDepositByPID(int idPartner)
        {
            List<PostStorageDetails> result = new List<PostStorageDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT a.*, v.format, t.typeEmplacement, p.idPointDepot, p.nomPointDepot, p.idPartenaire, p.numRue, p.nomRue, vi.ville 
                                FROM annonce a  
                                JOIN ville vi ON a.idVilleDepot = vi.idVille
                                JOIN emplacement e ON a.idEmplacement = e.idEmplacement 
                                JOIN volume v ON e.idVolume = v.idVolume 
                                JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement 
                                JOIN pointdedepot p ON e.idPointDepot = p.idPointDepot 
                                WHERE p.idPartenaire = @idPartner
                                AND (dateValidation IS NOT NULL AND dateValidation NOT LIKE '0001-01-01%') 
                                AND (dateDepot IS NULL OR dateDepot LIKE '0001-01-01%');";

            cmd.Parameters.Add(new MySqlParameter("idPartner", idPartner));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostStorageDetails postStorageDetails = new PostStorageDetails(post);
                    if (!dr.IsDBNull(dr.GetOrdinal("idPointDepot")))
                    {
                        postStorageDetails.IdPickupPoint = dr.GetInt32(dr.GetOrdinal("idPointDepot"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("format")))
                    {
                        postStorageDetails.StorageFormat = dr.GetString(dr.GetOrdinal("format"));
                    }if (!dr.IsDBNull(dr.GetOrdinal("typeEmplacement")))
                    {
                        postStorageDetails.StorageType = dr.GetString(dr.GetOrdinal("typeEmplacement"));
                    }if (!dr.IsDBNull(dr.GetOrdinal("nomPointDepot")))
                    {
                        postStorageDetails.PickupPointName = dr.GetString(dr.GetOrdinal("nomPointDepot"));
                    }if (!dr.IsDBNull(dr.GetOrdinal("numRue")))
                    {
                        postStorageDetails.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
                    }if (!dr.IsDBNull(dr.GetOrdinal("nomRue")))
                    {
                        postStorageDetails.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));
                    }if (!dr.IsDBNull(dr.GetOrdinal("ville")))
                    {
                        postStorageDetails.CityName = dr.GetString(dr.GetOrdinal("ville"));
                    }
                    result.Add(postStorageDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<PostStorageDetails> GetAllPostStorageDetailsAwaitingPickupByPID(int idPartner)
        {
            List<PostStorageDetails> result = new List<PostStorageDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT a.*, v.format, t.typeEmplacement, p.idPointDepot, p.nomPointDepot, p.idPartenaire, p.numRue, p.nomRue, vi.ville 
                                FROM annonce a  
                                JOIN ville vi ON a.idVilleDepot = vi.idVille
                                JOIN emplacement e ON a.idEmplacement = e.idEmplacement 
                                JOIN volume v ON e.idVolume = v.idVolume 
                                JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement 
                                JOIN pointdedepot p ON e.idPointDepot = p.idPointDepot 
                                WHERE p.idPartenaire = @idPartner
                                AND (dateDepot IS NOT NULL AND dateDepot NOT LIKE '0001-01-01%') 
                                AND (dateCollecte IS NULL OR dateCollecte LIKE '0001-01-01%');";

            cmd.Parameters.Add(new MySqlParameter("idPartner", idPartner));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostStorageDetails postStorageDetails = new PostStorageDetails(post);
                    if (!dr.IsDBNull(dr.GetOrdinal("idPointDepot")))
                    {
                        postStorageDetails.IdPickupPoint = dr.GetInt32(dr.GetOrdinal("idPointDepot"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("format")))
                    {
                        postStorageDetails.StorageFormat = dr.GetString(dr.GetOrdinal("format"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("typeEmplacement")))
                    {
                        postStorageDetails.StorageType = dr.GetString(dr.GetOrdinal("typeEmplacement"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("nomPointDepot")))
                    {
                        postStorageDetails.PickupPointName = dr.GetString(dr.GetOrdinal("nomPointDepot"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("numRue")))
                    {
                        postStorageDetails.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("nomRue")))
                    {
                        postStorageDetails.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("ville")))
                    {
                        postStorageDetails.CityName = dr.GetString(dr.GetOrdinal("ville"));
                    }
                    result.Add(postStorageDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllPastDonationsByID(int idSubscriber)
        {
            List<Post> result = new List<Post>();

            DateOnly currentDate = DateOnly.FromDateTime(DateTime.Today);
            string sqlDate = currentDate.ToString("yyyy-MM-dd");

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE idAdherent=@idSubscriber 
                                AND (dateDepot IS NOT NULL AND dateDepot NOT LIKE '0001-01-01%')
                                AND (dateDepot < @currentDate);";

            cmd.Parameters.Add(new MySqlParameter("idSubscriber", idSubscriber));
            cmd.Parameters.Add(new MySqlParameter("currentDate", sqlDate));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCurrentExchanges()
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (dateValidation IS NOT NULL AND dateValidation NOT LIKE '0001-01-01%')
                                AND (dateCollecte IS NULL OR dateCollecte LIKE '0001-01-01%');";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCancelled()
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (dateAnnulation IS NOT NULL AND dateAnnulation NOT LIKE '0001-01-01%');";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCompleted()
        {
            List<Post> result = new List<Post>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (dateCollecte IS NOT NULL AND dateCollecte NOT LIKE '0001-01-01%');";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllPublishedThisMonth()
        {
            List<Post> result = new List<Post>();

            String currentYear = DateTime.Now.Year.ToString();
            String currentMonth = DateTime.Now.Month.ToString();
            string sqlDate = currentYear + "-" + currentMonth + "-" + "01";

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (datePublication IS NOT NULL AND datePublication NOT LIKE '0001-01-01%')
                                AND (datePublication > @firstDayOfMonth);";

            cmd.Parameters.Add(new MySqlParameter("firstDayOfMonth", sqlDate));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCancelledThisMonth()
        {
            List<Post> result = new List<Post>();

            String currentYear = DateTime.Now.Year.ToString();
            String currentMonth = DateTime.Now.Month.ToString();
            string sqlDate = currentYear + "-" + currentMonth + "-" + "01";

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (dateAnnulation IS NOT NULL AND dateAnnulation NOT LIKE '0001-01-01%')
                                AND (dateAnnulation > @firstDayOfMonth);";

            cmd.Parameters.Add(new MySqlParameter("firstDayOfMonth", sqlDate));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<Post> GetAllCompletedThisMonth()
        {
            List<Post> result = new List<Post>();

            String currentYear = DateTime.Now.Year.ToString();
            String currentMonth = DateTime.Now.Month.ToString();
            string sqlDate = currentYear + "-" + currentMonth + "-" + "01";

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM dld_bdd.annonce
                                WHERE (dateCollecte IS NOT NULL AND dateCollecte NOT LIKE '0001-01-01%')
                                AND (dateCollecte > @firstDayOfMonth);";

            cmd.Parameters.Add(new MySqlParameter("firstDayOfMonth", sqlDate));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    result.Add(post);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public PostStorageDetails GetPostStorageDetailsByID(int idPost)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT a.*, v.format, t.typeEmplacement, p.idPointDepot, p.nomPointDepot, p.idPartenaire, p.numRue, p.nomRue, vi.ville 
                                FROM annonce a  
                                JOIN ville vi ON a.idVilleDepot = vi.idVille
                                JOIN emplacement e ON a.idEmplacement = e.idEmplacement 
                                JOIN volume v ON e.idVolume = v.idVolume 
                                JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement 
                                JOIN pointdedepot p ON e.idPointDepot = p.idPointDepot 
                                WHERE a.idAnnonce = @idAnnonce;";

            cmd.Parameters.Add(new MySqlParameter("idAnnonce", idPost));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    Post post = DataReaderToEntity(dr);
                    PostStorageDetails postStorageDetails = new PostStorageDetails(post);
                    if (!dr.IsDBNull(dr.GetOrdinal("idPointDepot")))
                    {
                        postStorageDetails.IdPickupPoint = dr.GetInt32(dr.GetOrdinal("idPointDepot"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("format")))
                    {
                        postStorageDetails.StorageFormat = dr.GetString(dr.GetOrdinal("format"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("typeEmplacement")))
                    {
                        postStorageDetails.StorageType = dr.GetString(dr.GetOrdinal("typeEmplacement"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("nomPointDepot")))
                    {
                        postStorageDetails.PickupPointName = dr.GetString(dr.GetOrdinal("nomPointDepot"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("numRue")))
                    {
                        postStorageDetails.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("nomRue")))
                    {
                        postStorageDetails.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));
                    }
                    if (!dr.IsDBNull(dr.GetOrdinal("ville")))
                    {
                        postStorageDetails.CityName = dr.GetString(dr.GetOrdinal("ville"));
                    }

                    return postStorageDetails;
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des annonces : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return null;
        }

    }
}
