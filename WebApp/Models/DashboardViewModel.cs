﻿using Fr.EQL.AI110.DLD_GD.Entities;

namespace WebApp.Models
{
    public class DashboardViewModel
    {
        #region ENTITIES
        public Subscriber Subscriber { get; set; }
        public Partner Partner { get; set; }
        public Partner PickupPoint { get; set; }
        public Partner Storage { get; set; }
        public Partner Post { get; set; }

        public int NbCurrentPosts { get; set; }
        public int NbUnconfirmedPosts { get; set; }
        public int NbUndepositedPosts { get; set; }
        public int NbCurrentReservations{ get; set; }
        public int NbConfirmedDeposits { get; set; }
        public int NbPastReservations { get; set; }
        public int NbPastDonations { get; set; } = 0;
        public int PpOccupancyNb { get; set; } = 0;
        #endregion

        #region LISTS
        public List<Partner> Partners { get; set; }
        public List<Subscriber> Subscribers { get; set; }
        public List<Post> CurrentPosts { get; set; }
        public List<Post> UnconfirmedPosts { get; set; }
        public List<Post> UndepositedPosts { get; set; }
        public List<Post> CurrentReservations { get; set; }
        public List<Post> ConfirmedDeposits { get; set; }
        public List<Post> PastReservations { get; set; }
        public List<Post> PastDonations { get; set; }
        public List<Post> AllCompletedExchanges { get; set; }
        public List<Post> AllCancelledPosts { get; set; }
        public List<Post> AllOngoingExchanges { get; set; }
        public List<Post> AllCancelledThisMonth { get; set; }
        public List<Post> AllPostsPublishedThisMonth { get; set; }
        public List<Post> AllCompletedExchangesThisMonth { get; set; }
        public List<PickupPointDetails> PickupPoints { get; set; }
        public List<Storage> Storages { get; set; }
        public List<PostStorageDetails> UndepositedPostStorageDetails { get; set; }
        public List<PostStorageDetails> UncollectedPostStorageDetails { get; set; }
        public List<PostStorageDetails> PpOccupancy { get; set; }
        #endregion
    }
}
