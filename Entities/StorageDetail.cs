﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class StorageDetail:Storage
    {
        public string Type { get; set; }
        public string Format { get; set; }

        public StorageDetail(Storage storage)
        {
            Id = storage.Id;
            IdVolume = storage.IdVolume;
            IdStorageType = storage.IdStorageType;
            IdPickupPoint = storage.IdPickupPoint;
            CreationDate = storage.CreationDate;
            DeletionDate = storage.DeletionDate;
            CancellationDate = storage.CancellationDate;
        }
        public StorageDetail()
        {
        }
    }
}
