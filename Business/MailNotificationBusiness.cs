﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MailKit.Net.Smtp;
using MimeKit;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class MailNotificationBusiness
    {
        public void NewReservationMail(Subscriber donator)
        {
            MimeMessage mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("DLD", "datelimitededon@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(donator.Name, donator.Email));
            mailMessage.Subject = "Réservation de votre don";
            mailMessage.Body = new TextPart("plain")
            {
                Text = "Bonjour " + donator.Name + ",\n\nUn adhérent souhaite réserver l'un de vos dons. " +
                "Veuillez accéder à l'application pour accepter ou refuser l'échange.\n\n" +
                "Toute l'équipe DLD vous remercie pour votre aide dans la lutte contre le gaspillage.\n\n" +
                "Bien à vous,\n" +
                "L'équipe DLD"
            };

            try
            {
                using (var smtpClient = new SmtpClient())
                    {
                        smtpClient.Connect("smtp.gmail.com", 465, true);
                        smtpClient.Authenticate("datelimitededon@gmail.com", "@DLD0000");
                        smtpClient.Send(mailMessage);
                        smtpClient.Disconnect(true);
                    }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur d'envoi" + ex.Message);
            }
            
        }

        public void NewValidationMail(Subscriber recipient, string decision)
        {
            MimeMessage mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("DLD", "datelimitededon@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(recipient.Name, recipient.Email));
            mailMessage.Subject = "Votre demande de réservation";
            mailMessage.Body = new TextPart("plain")
            {
                Text = "Bonjour " + recipient.Name + ",\n\nLe donateur a " + decision +
                " votre demande de réservation.\n" +
                "Consultez l'application pour voir le don ou en chercher d'autres.\n\n" +
                "Toute l'équipe DLD vous remercie pour votre aide dans la lutte contre le gaspillage.\n\n" +
                "Bien à vous,\n" +
                "L'équipe DLD"
            };

            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Connect("smtp.gmail.com", 465, true);
                    smtpClient.Authenticate("datelimitededon@gmail.com", "@DLD0000");
                    smtpClient.Send(mailMessage);
                    smtpClient.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur d'envoi" + ex.Message);
            }
        }

        public void NewDepositMail(Subscriber recipient, PostStorageDetails post)
        {
            MimeMessage mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("DLD", "datelimitededon@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(recipient.Name, recipient.Email));
            mailMessage.Subject = "Votre demande de réservation";
            mailMessage.Body = new TextPart("plain")
            {
                Text = "Bonjour " + recipient.Name + ",\n\nVotre don est désormais disponible à la collecte !\n"+
                "Vous pouvez le récupérer en point de dépôt :\n\n" +
                post.PickupPointName +"\n" + post.StreetNumber + " " + post.StreetName + "\n" + post.ZipCode + " " + post.CityName + "\n\n" +
                "Consultez l'application pour voir le don ou en chercher d'autres.\n\n" +
                "Toute l'équipe DLD vous remercie pour votre aide dans la lutte contre le gaspillage.\n\n" +
                "Bien à vous,\n" +
                "L'équipe DLD"
            };

            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Connect("smtp.gmail.com", 465, true);
                    smtpClient.Authenticate("datelimitededon@gmail.com", "@DLD0000");
                    smtpClient.Send(mailMessage);
                    smtpClient.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur d'envoi" + ex.Message);
            }
        }

        public void NewCollectionMail(Subscriber donator, PostStorageDetails post)
        {
            MimeMessage mailMessage = new MimeMessage();
            mailMessage.From.Add(new MailboxAddress("DLD", "datelimitededon@gmail.com"));
            mailMessage.To.Add(new MailboxAddress(donator.Name, donator.Email));
            mailMessage.Subject = "Votre demande de réservation";
            mailMessage.Body = new TextPart("plain")
            {
                Text = "Bonjour " + donator.Name + ",\n\nVotre don de '" + post.Title + "' a bien été récupéré." +
                "\n\nConsultez l'application pour voir vos dons ou effectuer une réservation.\n\n" +
                "Toute l'équipe DLD vous remercie pour votre aide dans la lutte contre le gaspillage.\n\n" +
                "Bien à vous,\n" +
                "L'équipe DLD"
            };

            try
            {
                using (var smtpClient = new SmtpClient())
                {
                    smtpClient.Connect("smtp.gmail.com", 465, true);
                    smtpClient.Authenticate("datelimitededon@gmail.com", "@DLD0000");
                    smtpClient.Send(mailMessage);
                    smtpClient.Disconnect(true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Erreur d'envoi" + ex.Message);
            }
        }

    }
}
