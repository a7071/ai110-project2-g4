﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class ProductTypeDAO : DAO
    {
        public ProductType GetProductType(int id)
        {
            ProductType result = new ProductType();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM typedeproduit 
                                              WHERE idTypeProduit = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    
                    result = DataReaderToEntity(dr);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }
        public List<ProductType> GetProductTypes()
        {
            List<ProductType> result = new List<ProductType>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM typedeproduit";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductType productType = new ProductType();
                    productType = DataReaderToEntity(dr);
                    result.Add(productType);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<ProductType> GetProductTypeBySubCategory(int idSubCategory)
        {
            List<ProductType> result = new List<ProductType>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT tp.* FROM typedeproduit tp LEFT JOIN souscategorie sc ON tp.idSousCategorie = sc.idSousCategorie
                                            WHERE (tp.idSousCategorie = @idSubCategory OR @idSubCategory = 0)";
            cmd.Parameters.Add(new MySqlParameter("idSubCategory", idSubCategory));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    ProductType productType = new ProductType();
                    productType = DataReaderToEntity(dr);
                    result.Add(productType);
                }

            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public ProductType DataReaderToEntity(DbDataReader dr)
        {
            ProductType productType = new ProductType();

            productType.Id = dr.GetInt32(dr.GetOrdinal("idTypeProduit"));
            productType.IdSubCategory = dr.GetInt32(dr.GetOrdinal("idSousCategorie"));
            productType.ProductTypeName = dr.GetString(dr.GetOrdinal("typeProduit"));
            return productType;
        }
    }
}
