﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class TokenPurchase
    {
        public int Id { get; set; }
        public int IdSubscriber { get; set; }
        public DateTime PurchaseDate { get; set; }
        public int TokensNumber { get; set; }
        public float PurchaseAmount { get; set; }

    }
}
