using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class PostDetails : Post
    {
        public List<Picture> Pictures { get; set; }
        public string CityName { get; set; }
        public string CityZipCode { get; set; }
        public int Distance { get; set; }
        public string Availability { get; set; } = "";
        public string DonatingSubscriberLogin { get; set; }
        public string PostCategory { get; set; }
        public ProductLineDetails ProductLineDetails { get; set; }
        public string ReceivingSubscriberLogin { get; set; }
        public bool IsRated { get; set; }



        public PostDetails(Post post)
        {
            Id = post.Id;
            IdDonatingSubscriber = post.IdDonatingSubscriber;
            IdReceivingSubscriber = post.IdReceivingSubscriber;
            IdStorage = post.IdStorage;
            PostDate = post.PostDate;
            Title = post.Title;
            Description = post.Description;
            Picture = post.Picture;
            ReservationDate = post.ReservationDate;
            ValidationDate = post.ValidationDate;
            DeletionDate = post.DeletionDate;
            DepositDate = post.DepositDate;
            CollectionDate = post.CollectionDate;
            PinCode = post.PinCode;
            IdCityOfDeposit = post.IdCityOfDeposit;
        }

    }
}
