﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Unavailability
    {
        public int Id { get; set; }
        public int IdPartner { get; set; }
        public int IdSubscriber { get; set; }
        public DateTime StartUnavaibilityDate { get; set; }
        public DateTime EndUnavaibilityDate { get; set; }

    }
}
