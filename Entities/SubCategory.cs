﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class SubCategory
    {
        public int Id { get; set; }
        public int IdCategory { get; set; }
        public string SubCategoryName { get; set; }

    }
}
