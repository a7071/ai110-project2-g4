﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class Unit
    {
        public int Id { get; set; }
        public string UnitName { get; set; }
    }
}
