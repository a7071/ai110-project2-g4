﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class ProductLineDAO : DAO
    {
        public void Create(ProductLine productLine)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO ligneproduit
                                            (idAnnonce,idProduit,idUnite,quantite,dlc)
                                            VALUES
                                            (@idPost,@idProduct,@idUnit,@quantity,@bestBeforeDate)";
            cmd.Parameters.Add(new MySqlParameter("idPost", productLine.IdPost));
            cmd.Parameters.Add(new MySqlParameter("idProduct", productLine.IdProduct));
            cmd.Parameters.Add(new MySqlParameter("idUnit", productLine.IdUnit));
            cmd.Parameters.Add(new MySqlParameter("quantity", productLine.Quantity));
            cmd.Parameters.Add(new MySqlParameter("bestBeforeDate", productLine.BestBeforeDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur de la publication d'une annonce : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

        }

        public ProductLine GetProductLine(int id)
        {
            ProductLine result = new ProductLine();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM ligneproduit
                                         WHERE (idAnnonce = @id)";
            cmd.Parameters.Add(new MySqlParameter("id", id));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de lecture de la base de données." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public ProductLineDetails GetProductLineDetails(int idPost)
        {
            ProductLineDetails result = new ProductLineDetails();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT p_line.* ,p.nomProduit, p_cat.idCategorie, p_cat.categorie
                                FROM dld_bdd.ligneproduit p_line
                                LEFT JOIN produit p ON p_line.idProduit = p.idProduit
                                LEFT JOIN typedeproduit p_type ON p.idTypeProduit = p_type.idTypeProduit
                                LEFT JOIN souscategorie p_souscat ON p_type.idSousCategorie = p_souscat.idSousCategorie
                                LEFT JOIN categorie p_cat ON p_souscat.idCategorie = p_cat.idCategorie
                                WHERE idAnnonce = @idPost
                                ";

            cmd.Parameters.Add(new MySqlParameter("idPost", idPost));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    ProductLine productLine = DataReaderToEntity(dr);
                    result = new ProductLineDetails(productLine);
                    result.ProductName = dr.GetString(dr.GetOrdinal("nomProduit"));
                    result.ProductCategoryId = dr.GetInt32(dr.GetOrdinal("idCategorie"));
                    result.ProductCategoryName = dr.GetString(dr.GetOrdinal("categorie"));
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération de la ligne produit : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

            return result;
        }

        public void DeleteProductLine(int id)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM ligneproduit
                                       WHERE idAnnonce = @id";
            cmd.Parameters.Add(new MySqlParameter("id", id));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
        }

        public ProductLine DataReaderToEntity(DbDataReader dr)
        {
            ProductLine result = new ProductLine();
            result.IdPost = dr.GetInt32(dr.GetOrdinal("idAnnonce"));
            result.IdProduct = dr.GetInt32(dr.GetOrdinal("idProduit"));
            result.IdUnit = dr.GetInt32(dr.GetOrdinal("idUnite"));
            result.Quantity = dr.GetFloat(dr.GetOrdinal("quantite"));
            result.BestBeforeDate = dr.GetDateTime(dr.GetOrdinal("dlc"));
            return result;
        }

    }
}
