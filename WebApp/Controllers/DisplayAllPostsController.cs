﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class DisplayAllPostsController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            PostSearchViewModel model = new PostSearchViewModel();

            PostBusiness postBU = new PostBusiness();
            model.Posts = postBU.GetPosts();

            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            RatingBusiness ratBu = new RatingBusiness();

            foreach (PostDetails post in model.Posts)
            {
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                if (subscriberBU.GetSubscriberById(post.IdReceivingSubscriber) != null)
                {
                    post.ReceivingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdReceivingSubscriber).Login;
                }
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                    if (ratBu.GetRatingByIdPost(post.IdDonatingSubscriber, post.Id).Id != 0)
                    {
                        post.IsRated = true;
                    }
                }

            }
            return View(model);
        }

        [HttpPost]
        public IActionResult Index(PostSearchViewModel model)
        {
            PostBusiness postBU = new PostBusiness();
            model.Posts = postBU.GetPostsByIdSubscriber(model.IdDonatingSubscriber);

            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            RatingBusiness ratBu = new RatingBusiness();

            foreach (PostDetails post in model.Posts)
            {
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                if (subscriberBU.GetSubscriberById(post.IdReceivingSubscriber) != null)
                {
                    post.ReceivingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdReceivingSubscriber).Login;
                }
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                    if (ratBu.GetRatingByIdPost(post.IdDonatingSubscriber, post.Id).Id != 0)
                    {
                        post.IsRated = true;
                    }
                }
            }
            return View(model);
        }
        [HttpGet]
        public IActionResult IndexReceiver()
        {
            PostSearchViewModel model = new PostSearchViewModel();

            PostBusiness postBU = new PostBusiness();
            model.Posts = postBU.GetPostsOfReceiver();

            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            foreach (PostDetails post in model.Posts)
            {
                RatingBusiness ratBu = new RatingBusiness();
                
                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                if (subscriberBU.GetSubscriberById(post.IdReceivingSubscriber) != null)
                {
                    post.ReceivingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdReceivingSubscriber).Login;
                    if (ratBu.GetRatingByIdPost(post.IdReceivingSubscriber, post.Id).Id != 0)
                    {
                        post.IsRated = true;
                    }
                }
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                }

            }
            return View(model);
        }
        [HttpPost]
        public IActionResult IndexReceiver(PostSearchViewModel model)
        {
            PostBusiness postBU = new PostBusiness();
            if (model.IdDonatingSubscriber != 0)
            {
            model.Posts = postBU.SearchPostsOfReceiver(model.Title, 0, 0, model.IdDonatingSubscriber);
            }
            else
            {
            model.Posts = postBU.GetPostsOfReceiver();
            }
           
            CityBusiness cityBU = new CityBusiness();
            model.Cities = cityBU.GetCities();

            SubscriberBusiness subscriberBU = new SubscriberBusiness();
            model.Subscribers = subscriberBU.GetSubscribers();

            foreach (PostDetails post in model.Posts)
            {
                RatingBusiness ratBu = new RatingBusiness();

                post.CityName = cityBU.GetCityById(post.IdCityOfDeposit).CityName;
                if (subscriberBU.GetSubscriberById(post.IdReceivingSubscriber) != null)
                {
                    post.ReceivingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdReceivingSubscriber).Login;
                    if (ratBu.GetRatingByIdPost(post.IdReceivingSubscriber, post.Id).Id != 0)
                    {
                        post.IsRated = true;
                    }
                }
                if (subscriberBU.GetSubscriberById(post.IdDonatingSubscriber) != null)
                {
                    post.DonatingSubscriberLogin = subscriberBU.GetSubscriberById(post.IdDonatingSubscriber).Login;
                }
            }
            return View(model);
        }

        public IActionResult DisplayPost(int id)
        {
            PostViewModel model = new PostViewModel();

            PostBusiness postBu = new PostBusiness();
            model.Post = postBu.GetPostById(id);

            ProductLineBusiness productLineBu = new ProductLineBusiness();
            model.ProductLine = productLineBu.GetProductLine(id);

            UnitBusiness unitBu = new UnitBusiness();
            model.Unit = unitBu.GetUnit(model.ProductLine.IdUnit);

            if (model.Post.IdStorage != 0)
            {
            StorageBusiness storageBusiness = new StorageBusiness();
            model.Storage = storageBusiness.GetStorage(model.Post.IdStorage);

            PickupPointBusiness pickupPointBusiness = new PickupPointBusiness();
            model.PickupPoint = pickupPointBusiness.GetPickupPoint(model.Storage.IdPickupPoint);
            }
           
            CityBusiness cityBusiness = new CityBusiness();
            model.City = cityBusiness.GetCityById(model.Post.IdCityOfDeposit);

            return View(model);
        }
        public IActionResult Delete(int id)
        {
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.DeletionDate = DateTime.Now;
            postBu.Update(post);
            return RedirectToAction("Index");
        }
        public IActionResult Validate(int id)
        {
            MailNotificationBusiness mailBU = new MailNotificationBusiness();
            SubscriberBusiness subBU = new SubscriberBusiness();
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.ValidationDate = DateTime.Now;
            // Si remise en main propre, date de dépot automatiquement remplie
            if (post.IdStorage == 0)
            {
                post.DepositDate = DateTime.Now;
            }
            postBu.Update(post);

            //envoie la décision par e-mail au bénéficiaire
            string decision = "validé";
            Subscriber recipient = subBU.GetSubscriberById(post.IdReceivingSubscriber);
            mailBU.NewValidationMail(recipient, decision);

            return RedirectToAction("Index");
        }
        public IActionResult Refuse(int id)
        {
            MailNotificationBusiness mailBU = new MailNotificationBusiness();
            SubscriberBusiness subBU = new SubscriberBusiness();

            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.ReservationDate = null;
            postBu.Update(post);

            //envoie la décision par e-mail au bénéficiaire
            string decision = "refusé";
            Subscriber subscriber = subBU.GetSubscriberById(post.IdReceivingSubscriber);
            mailBU.NewValidationMail(subscriber, decision);

            return RedirectToAction("Index");
        }
        public IActionResult Ok(int id)
        {
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.IdReceivingSubscriber = 0;
            postBu.Update(post);
            return RedirectToAction("IndexReceiver");
        }
        public IActionResult Cancel(int id)
        {
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.ReservationDate = null;
            post.ValidationDate = null;
            post.IdReceivingSubscriber = 0;
            postBu.Update(post);
            return RedirectToAction("IndexReceiver");
        }
        public IActionResult ConfirmedCollection(int id)
        {
            PostBusiness postBu = new PostBusiness();
            Post post = postBu.GetPostById(id);
            post.CollectionDate = DateTime.Now;
            postBu.Update(post);
            return RedirectToAction("IndexReceiver");
        }
    }
}
