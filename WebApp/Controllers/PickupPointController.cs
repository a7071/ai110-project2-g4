﻿using Fr.EQL.AI110.DLD_GD.Business;
using Fr.EQL.AI110.DLD_GD.Entities;
using Microsoft.AspNetCore.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class PickupPointController : Controller
    {
        [HttpGet]
        public IActionResult Index()
        {
            PickupPointViewModel model = new PickupPointViewModel();

            PickupPointBusiness ppBU = new PickupPointBusiness();
            @ViewBag.PickupPointsDetails = ppBU.GetAllPickupDetails();

            PartnerBusiness partnerBU = new PartnerBusiness();
            model.Partners = partnerBU.GetPartners();

            CityBusiness cityBU = new CityBusiness();
            ViewBag.Cities = cityBU.GetCities();

            return View(model);
        }


        [HttpPost]
        public IActionResult Index(PickupPointViewModel model)
        {
            
            PickupPointBusiness ppBU = new PickupPointBusiness();
            if(model.IdPartner != 0)
            {
                @ViewBag.PickupPointsDetails = ppBU.GetPickupPointsByIdPartner(model.IdPartner);
            }
            else
            {
                @ViewBag.PickupPointsDetails = ppBU.GetAllPickupDetails();
            }

            PartnerBusiness partnerBU = new PartnerBusiness();
            model.Partners = partnerBU.GetPartners();

            CityBusiness cityBU = new CityBusiness();
            ViewBag.Cities = cityBU.GetCities();

            return View(model);
        }



        [HttpGet]
        [Route("PickupPoint/Update/{pickupId}")]
        public IActionResult Update(int pickupId)
        {
            PickupPointViewModel model = new PickupPointViewModel();
            PickupPointBusiness ppBU = new PickupPointBusiness();
            model.Pickup = ppBU.GetPickupPoint(pickupId);
            model.PickupDetails = ppBU.GetPickupDetailsById(pickupId);

            CityBusiness cityBU = new CityBusiness();
            ViewBag.Cities = cityBU.GetCities();

            PartnerBusiness partnerBU = new PartnerBusiness();
            ViewBag.Partners = partnerBU.GetPartners();

            return View(model);
        }

        
        [HttpPost]
        [Route("PickupPoint/Update/{pickupId}")]
        public IActionResult Update(PickupPointViewModel model, int pickupId)
        {
            CityBusiness cityBU = new CityBusiness();
            ViewBag.Cities = cityBU.GetCities();

            if (model.Pickup.IdPartner != 0)
            {
                try
                {
                    PickupPointBusiness ppBU = new PickupPointBusiness();
                    model.Pickup.Id = pickupId;
                    ppBU.SavePickupPoint(model.Pickup);

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
            }
            else
            {
                return View(model);
            }
        }

        [Route("PickupPoint/Desactivate/{pickupId}")]
        public IActionResult Desactivate(int pickupId)
        {
            PickupPointBusiness bu = new PickupPointBusiness();
            PickupPoint pickup = bu.GetPickupPoint(pickupId);
            bu.DesactivatePickupPoint(pickup);
            return RedirectToAction("Index");
        }


        [HttpGet]
        public IActionResult Create()
        {
            CityBusiness cityBU = new CityBusiness();
            ViewBag.Cities = cityBU.GetCities();

            WeekdayBusiness weekdayBU = new WeekdayBusiness();
            ViewBag.Days = weekdayBU.GetWeekdays();

            PartnerBusiness partnerBU = new PartnerBusiness();
            ViewBag.Partners = partnerBU.GetPartners();

            return View();
        }


        [HttpPost]
        public IActionResult Create(PickupPointDetails model)
        {
            WeekdayBusiness weekdaybu = new WeekdayBusiness();
            List<Weekday> weekdays = weekdaybu.GetWeekdays();

            PickupPointBusiness ppbusiness = new PickupPointBusiness();
            ScheduleBusiness schedulebusiness = new ScheduleBusiness();

            if (model.IdPartner != 0)
            {
                try
                {
                    model.CreationDate = DateTime.Now;

                    ppbusiness.SavePickupPoint(model);
                    int pickupId = model.Id;


                    foreach (Weekday day in weekdays)
                    {
                        Schedule schedule = model.Planning[day.Day];
                        schedule.IdDay = day.Id;
                        schedule.IdPickupPoint = model.Id;

                        schedulebusiness.InsertSchedule(schedule);
                    }

                    return RedirectToAction("Index");
                }
                catch (Exception ex)
                {
                    ViewBag.ErrorMessage = ex.Message;
                    return View("Error");
                }
            }
            else
            {
                return View(model);
            }

        }


        /*
        [HttpGet]
        [Route("PickupPoint/MyPickupPoints/{userID}")]
        public IActionResult MyPickupPoints(int userID)
        {
            PickupPointViewModel model = new PickupPointViewModel();

            PickupPointBusiness ppBU = new PickupPointBusiness();
            model.PickupPoints = ppBU.GetPickupPointsByIdPartner(userID);
            WeekdayBusiness weekdayBU = new WeekdayBusiness();

            ScheduleBusiness scheduleBU = new ScheduleBusiness();
            foreach (PickupPointDetails pickupPointDetails in model.PickupPoints)
            {
                pickupPointDetails.ScheduleDetailsList = scheduleBU.GetSchedulesByIdPickup(pickupPointDetails.Id);
                foreach (ScheduleDetails scheduledetails in pickupPointDetails.ScheduleDetailsList)
                {
                    scheduledetails.DayName = weekdayBU.GetDayById(scheduledetails.IdDay).Day;
                }
            }

            return View(model);
        }*/



        [HttpGet]
        public IActionResult Details(int id)
        {
            PickupPointBusiness pickupBU = new PickupPointBusiness();
            PickupPointDetails pickupPointDetails = pickupBU.GetPickupDetailsById(id);

            WeekdayBusiness weekdayBU = new WeekdayBusiness();
            CityBusiness cityBU = new CityBusiness();
            ScheduleBusiness scheduleBU = new ScheduleBusiness();

            pickupPointDetails.ZipCode = cityBU.GetCityById(pickupPointDetails.IdCity).ZipCode;
            pickupPointDetails.CityName = cityBU.GetCityById(pickupPointDetails.IdCity).CityName;
            pickupPointDetails.ScheduleDetailsList = scheduleBU.GetSchedulesByIdPickup(pickupPointDetails.Id);
            foreach (ScheduleDetails scheduledetails in pickupPointDetails.ScheduleDetailsList)
            {
                scheduledetails.DayName = weekdayBU.GetDayById(scheduledetails.IdDay).Day;
            }

            return View(pickupPointDetails);
        }


    }
       
 }
