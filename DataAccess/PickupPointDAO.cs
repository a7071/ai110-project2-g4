﻿using Fr.EQL.AI110.DLD_GD.Entities;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.DataAccess
{
    public class PickupPointDAO : DAO
    {
        public PickupPoint InsertPickupPoint(PickupPoint pickup)
        {
            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO pointdedepot
                                (idPointDepot, idPartenaire, idVille, nomPointDepot, numRue, nomRue, dateCreation, dateRetrait)
                                VALUES
                                (@id, @idPartner, @idCity, @pickupPointName, @streetNumber, @streetName, @creationDate, @deletionDate);
                                SELECT LAST_INSERT_ID();";

            cmd.Parameters.Add(new MySqlParameter("id", pickup.Id));
            cmd.Parameters.Add(new MySqlParameter("idPartner", pickup.IdPartner));
            cmd.Parameters.Add(new MySqlParameter("idCity", pickup.IdCity));
            cmd.Parameters.Add(new MySqlParameter("pickupPointName", pickup.PickupPointName));
            cmd.Parameters.Add(new MySqlParameter("streetName", pickup.StreetName));
            cmd.Parameters.Add(new MySqlParameter("streetNumber", pickup.StreetNumber));
            cmd.Parameters.Add(new MySqlParameter("creationDate", pickup.CreationDate));
            cmd.Parameters.Add(new MySqlParameter("deletionDate", pickup.DeletionDate));

            try
            {
                cnx.Open();

                pickup.Id = Convert.ToInt32(cmd.ExecuteScalar());
            }
            catch (MySqlException exc)
            {
                throw new Exception("Une erreur est survenue lors de l'insertion dans la base d'un point de dépôt." + exc.Message);
            }
            finally
            {
                cnx.Close();

            }
            return pickup;
        }


        public void Update(PickupPoint pickup)
        {
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE pointdedepot
                                SET idPartenaire = @idPartner,
                                    idVille = @idCity,
                                    nomPointDepot = @pickupPointName,
                                    numRue = @streetNumber,  
                                    nomRue = @streetName,
                                    dateCreation = @creationDate,
                                    dateRetrait = @deletionDate
                                WHERE idPointDepot = @idPickupPoint";
            

            cmd.Parameters.Add(new MySqlParameter("idPartner", pickup.IdPartner));
            cmd.Parameters.Add(new MySqlParameter("idCity", pickup.IdCity));
            cmd.Parameters.Add(new MySqlParameter("pickupPointName", pickup.PickupPointName));
            cmd.Parameters.Add(new MySqlParameter("streetNumber", pickup.StreetNumber));
            cmd.Parameters.Add(new MySqlParameter("streetName", pickup.StreetName));
            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", pickup.Id));
            cmd.Parameters.Add(new MySqlParameter("creationDate", pickup.CreationDate));
            cmd.Parameters.Add(new MySqlParameter("deletionDate", pickup.DeletionDate));

            try
            {
                cnx.Open();
                cmd.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur de la modification d'un point de dépôt : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }

        }

      

        public PickupPoint GetPickupPoint(int id)
        {
            PickupPoint result = null;
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM pointdedepot
                                         WHERE (idPointDepot = @id)";
            cmd.Parameters.Add(new MySqlParameter("id",@id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                if (dr.Read())
                {
                    result = DataReaderToEntity(dr);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<PickupPoint> GetPickupPoints()
        {
            List<PickupPoint> result = new List<PickupPoint>();
            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT * FROM pointdedepot";
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PickupPoint pickup = DataReaderToEntity(dr);
                    result.Add(pickup);
                }
            }
            catch(MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<PickupPoint> GetPickupPointsByMultiCriteria(int idCity,int idVolume, int idType)
        {
            List<PickupPoint> result = new List<PickupPoint>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"SELECT DISTINCT p.* FROM pointdedepot p JOIN ville vi ON vi.idVille = p.idVille
                                                               JOIN emplacement e ON p.idPointDepot = e.idPointDepot
                                                                      JOIN volume vo ON e.idVolume = vo.idVolume
                                                                      JOIN typed_emplacement t ON e.idTypeEmplacement = t.idTypeEmplacement
                                           WHERE (p.idVille = @idCity)
                                           AND (e.idVolume = @idVolume)
                                           AND (e.idTypeEmplacement = @idType)";
            cmd.Parameters.Add(new MySqlParameter("idCity",idCity));
            cmd.Parameters.Add(new MySqlParameter("idVolume",idVolume));
            cmd.Parameters.Add(new MySqlParameter("idType",idType));
            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    PickupPoint pickup = DataReaderToEntity(dr);
                    result.Add(pickup);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur" + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<PickupPointDetails> GetByIdPartner(int id)
        {
            List<PickupPointDetails> result = new List<PickupPointDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM pointdedepot WHERE idPartenaire = @idPartner";

            cmd.Parameters.Add(new MySqlParameter("idPartner", id));

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    PickupPoint pickupPoint = DataReaderToEntity(dr);
                    PickupPointDetails pickupPointDetails = new PickupPointDetails(pickupPoint);
                    result.Add(pickupPointDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des points de dépôt : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public List<PickupPointDetails> GetAllPickupDetails()
        {
            List<PickupPointDetails> result = new List<PickupPointDetails>();

            DbConnection cnx = new MySqlConnection(CNX_STR);
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = "SELECT * FROM pointdedepot";

            try
            {
                cnx.Open();
                DbDataReader dr = cmd.ExecuteReader();

                while (dr.Read())
                {
                    PickupPoint pickupPoint = DataReaderToEntity(dr);
                    PickupPointDetails pickupPointDetails = new PickupPointDetails(pickupPoint);
                    result.Add(pickupPointDetails);
                }
            }
            catch (MySqlException ex)
            {
                throw new Exception("Erreur lors de la récupération des points de dépôt : " + ex.Message);
            }
            finally
            {
                cnx.Close();
            }
            return result;
        }

        public PickupPointDetails GetPickupDetails(int id)
        {
            PickupPointDetails pickupDetails = new PickupPointDetails(); 

            DbConnection cnx = new MySqlConnection();

            cnx.ConnectionString = CNX_STR;

            DbCommand cmd = cnx.CreateCommand();

            cmd.CommandText = @"SELECT * FROM pointdedepot 
                                WHERE idPointDepot = @idPickupPoint";

            cmd.Parameters.Add(new MySqlParameter("idPickupPoint", id));

            try
            {
                cnx.Open();

                DbDataReader dr = cmd.ExecuteReader();

                if (dr.Read())
                {
                   PickupPoint pickup = DataReaderToEntity(dr);
                    pickupDetails = new PickupPointDetails(pickup);
                }
            }
            catch (MySqlException exc)
            {
                throw new Exception("Erreur de lecture de la base de données." + exc.Message);
            }
            finally
            {
                cnx.Close();
            }

            return pickupDetails;
        }


        private PickupPoint DataReaderToEntity(DbDataReader dr)
        {
            PickupPoint pickup = new PickupPoint();

            pickup.Id = dr.GetInt32(dr.GetOrdinal("idPointDepot"));
            pickup.IdPartner = dr.GetInt32(dr.GetOrdinal("idPartenaire"));
            pickup.IdCity = dr.GetInt32(dr.GetOrdinal("idVille"));
            pickup.PickupPointName = dr.GetString(dr.GetOrdinal("nomPointDepot"));
            pickup.StreetNumber = dr.GetString(dr.GetOrdinal("numRue"));
            pickup.StreetName = dr.GetString(dr.GetOrdinal("nomRue"));

            if (!dr.IsDBNull(dr.GetOrdinal("dateCreation")))
            {
                pickup.CreationDate = dr.GetDateTime(dr.GetOrdinal("dateCreation"));
            }
            if (!dr.IsDBNull(dr.GetOrdinal("dateRetrait")))
            {
                pickup.DeletionDate = dr.GetDateTime(dr.GetOrdinal("dateRetrait"));
            }

            return pickup;
        }


    }
}
