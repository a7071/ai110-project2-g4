﻿using Fr.EQL.AI110.DLD_GD.DataAccess;
using Fr.EQL.AI110.DLD_GD.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Business
{
    public class PictureBusiness
    {
        public List<Picture> GetPictures()
        {
            PictureDAO pictDAO = new PictureDAO();
            return pictDAO.GetAll();
        }

        public List<Picture> GetPicturesById(int id)
        {
            PictureDAO pictDAO = new PictureDAO();
            return pictDAO.GetById(id);
        }

        public void SavePicture(Picture picture)
        {
            PictureDAO pictDAO = new PictureDAO();
            pictDAO.Insert(picture);
        }

        public void DeletePicture(int id)
        {
            PictureDAO pictDAO = new PictureDAO();
            pictDAO.Delete(id);
        }
    }
}
