﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class PurchaseLine
    {
        public int IdTokenPurchase { get; set; }
        public int IdTokenBundle { get; set; }
        public int Quantity { get; set; }

    }
}
