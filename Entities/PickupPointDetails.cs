﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fr.EQL.AI110.DLD_GD.Entities
{
    public class PickupPointDetails : PickupPoint
    {
        public List<ScheduleDetails> ScheduleDetailsList { get; set; }
        public Dictionary<string, Schedule> Planning { get; set; } = new Dictionary<string, Schedule>();
        public string ZipCode { get; set; }
        public string CityName { get; set; }
        public Partner PickupPartner { get; set; }
        public List<StorageDetail> Storages { get; set; }

        public PickupPointDetails()
        {
            ScheduleDetailsList = new List<ScheduleDetails>();
        }
        public PickupPointDetails(PickupPoint pickupPoint)
        {
            Id = pickupPoint.Id;
            IdPartner = pickupPoint.IdPartner;
            IdCity = pickupPoint.IdCity;
            PickupPointName = pickupPoint.PickupPointName;
            StreetName = pickupPoint.StreetName;
            StreetNumber = pickupPoint.StreetNumber;
            CreationDate = pickupPoint.CreationDate;
            DeletionDate = pickupPoint.DeletionDate;
        }

    }
}
